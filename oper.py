# sidfam/oper.py -- Operators defined on stateful automaton.
# Created by Correctizer on Jul 11, 2018.

from core import merge_trans, determine_auto, merge_hop


def concatenation(auto1, auto2):
    def prefix(n, origin):
        return f'<concat>[{n}]({origin})'

    nodes_table = {}
    for node, edges in auto1['nodes'].items():
        next_edges = []
        for edge in edges:
            if edge['dest'] != auto1['end']:
                next_dest = prefix(1, edge['dest'])
                next_hop = edge['trans']['hop']
            else:
                assert edge['trans']['hop'] in ['e', 'd']
                if edge['trans']['hop'] == 'e':
                    next_dest = prefix(2, auto2['initial'])
                    next_hop = '.'
                else:
                    next_dest = prefix(2, auto2['end'])
                    next_hop = 'd'
            next_edges.append({
                **edge,  # no more items for now, but who knows
                'dest': next_dest,
                'trans': {**edge['trans'], 'hop': next_hop},
            })
        if node == auto1['end']:
            assert len(next_edges) == 0
        else:
            nodes_table[prefix(1, node)] = next_edges
    for node, edges in auto2['nodes'].items():
        next_edges = [{**edge, 'dest': prefix(2, edge['dest'])} for edge in edges]
        nodes_table[prefix(2, node)] = next_edges
    auto3 = {
        'initial': prefix(1, auto1['initial']), 'end': prefix(2, auto2['end']),
        'nodes': nodes_table,    
    }
    return determine_auto(auto3)

def intersection(auto1, auto2):
    def name(node1, node2):
        return f'<inter>({node1}, {node2})'

    nodes_table = {}
    for node1, edges1 in auto1['nodes'].items():
        for node2, edges2 in auto2['nodes'].items():
            node3 = name(node1, node2)
            assert node3 not in nodes_table
            for edge1 in edges1:
                dest1, hop1 = edge1['dest'], edge1['trans']['hop']
                for edge2 in edges2:
                    dest2, hop2 = edge2['dest'], edge2['trans']['hop']
                    if merge_hop(hop1, hop2) is not None:
                        if dest1 == auto1['end'] or dest2 == auto2['end']:
                            dest3 = '<inter>(qe)'
                        else:
                            dest3 = name(dest1, dest2)
                        edge3 = {
                            'dest': dest3,
                            'trans': merge_trans(edge1['trans'], edge2['trans'])
                        }
                        if node3 not in nodes_table:
                            nodes_table[node3] = []
                        nodes_table[node3].append(edge3)
    auto3 = {
        'initial': name(auto1['initial'], auto2['initial']), 'end': '<inter>(qe)',
        'nodes': nodes_table,
    }
    return determine_auto(auto3)

def symmetric_difference(auto1, auto2):
    nodes_table = {}
    def install_nodes(auto, n):
        for node, edges in auto['nodes'].items():
            next_edges = []
            for edge in edges:
                if edge['dest'] == auto1['end']:
                    assert edge['trans']['hop'] in ['e', 'd']
                    next_dest = '<symdif>(qe)'
                else:
                    next_dest = f'<symdif>[{n}]({edge["dest"]})'
                next_edges.append({**edge, 'dest': next_dest})
            nodes_table[f'<symdif>[{n}]({node})'] = next_edges

    install_nodes(auto1, 1)
    install_nodes(auto2, 2)

    auto3 = {
        'initial': '<symdif>(q0)', 'end': '<symdif>(qe)',
        'nodes': {
            **nodes_table,
            '<symdif>(q0)': [
                {
                    'dest': f'<symdif>[1]({auto1["initial"]})',
                    'trans': 'epsilon',
                }, {
                    'dest': f'<symdif>[2]({auto2["initial"]})',
                    'trans': 'epsilon',
                },
            ],
        },
    }
    return determine_auto(auto3)

def union(auto1, auto2):
    return symmetric_difference(
        intersection(auto1, auto2),
        symmetric_difference(auto1, auto2),
    )
