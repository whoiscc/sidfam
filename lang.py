# sidfam/lang.py -- Simple frontend for several SDN language.
# Created by Correctizer on Jul 12, 2018.

from core import determine_auto, merge_trans, merge_comp_list, negate_comp, \
    Resource
from oper import symmetric_difference
from util import current_hop_requirement

# ml: dict
#   path: dict
#     <path name>: dict
#       src: <ip> -> bool
#       dst: <ip> -> bool
#       pkt: [<pkt>]
#       regex: <path>
#   rule: [<rule>]
# rule: dict
#   type: 'min' | 'max'
#   path: [<path name>]
#   limit: <factor> 
# bw: dict
#   (<ip>, <ip>): <res>

def merlin(ml):
    bw = {}
    bw_initialized = False

    def bw_getter(bw_dict):
        nonlocal bw_initialized
        bw_initialized = True
        bw.clear()
        for res, rat in bw_dict.items():
            bw[res] = rat

    path_table, rule_list = ml['path'], ml['rule']
    path_req = {path: 0 for path in path_table}
    for rule in rule_list:
        if rule['type'] == 'max':
            raise Exception('Max rule of Merlin is not supported')
        elif rule['type'] == 'min':
            assert len(rule['path']) != 0
            if len(rule['path']) > 1:
                raise Exception('Combinated rule of Merlin is not supported')
            path = rule['path'][0]
            if path not in path_req:
                raise Exception(f'Unknown path name {path}')
            path_req[path] = max(path_req[path], rule['limit'])
    auto = None

    # TODO: Move bandwidth code down.
    def bw_req(env):
        if not bw_initialized:
            raise Exception(
                'Solving problem before receiving bandwidth infomation')
        return {
            bw[env['current_hop'], env['next_hop']]: path_req[path]
        }

    for path, spec in path_table.items():
        trans = {
            'src': spec.get('src', lambda ip: True), 
            'dst': spec.get('dst', lambda ip: True),
            'pkt': spec.get('pkt', []),
            'hop': '.', 'spg': [], 'spu': [],
            'req': lambda env: {},
        }
        sub_nodes_table = merlin_regex_to_nodes_table(
            spec['regex'], '<merlin>+', '<merlin>(q0)', "<merlin>(qe')", trans)
        sub_auto = determine_auto({
            'initial': '<merlin>(q0)', 'end': '<merlin>(qe)',
            'nodes': {
                **sub_nodes_table,
                "<merlin>(qe')": [
                    *sub_nodes_table.get("<merlin>(qe')", []),
                    {
                        'dest': '<merlin>(qe)',
                        'trans': {
                            **trans, 'hop': 'e',
                        },
                    },
                ],
            },
        })
        add_direct_edge(sub_auto)
        bw_req_trans = {
            'src': lambda ip: True, 'dst': lambda ip: True,
            'pkt': [], 'spg': [], 'spu': [], 'hop': '.', 
            'req': bw_req,
        }
        for node in sub_auto['nodes']:
            next_edges = []
            for edge in sub_auto['nodes'][node]:
                if edge['dest'] != sub_auto['end']:
                    next_edges.append({
                        **edge, 
                        'trans': merge_trans(edge['trans'], bw_req_trans)
                    })
                else:
                    next_edges.append(edge)
            sub_auto['nodes'][node] = next_edges
        # from pprint import pprint
        # pprint(sub_auto)
        if auto is None:
            auto = sub_auto
        else:
            auto = symmetric_difference(auto, sub_auto)
    # from pprint import pprint
    # pprint(auto)
    return determine_auto(auto), bw_getter

def add_direct_edge(auto):
    before_end = set()
    for node in auto['nodes']:
        assert auto['nodes'][node] != []
        for edge in auto['nodes'][node]:
            if edge['dest'] == auto['end']:
                assert edge['trans']['hop'] == 'e'
                before_end.add(node)
                break
    # print(before_end)
    for node in auto['nodes']:
        for edge in auto['nodes'][node]:
            if edge['dest'] in before_end:
                # print('add direct edge')
                auto['nodes'][node].append({
                    'dest': auto['end'],
                    'trans': {**edge['trans'], 'hop': 'e'},
                })

# regex: dict
#   type: 'hop' | 'auto' | 'exclude' | 'sequence' | 'star' | 'or'
#   value: <depends on type>
#     'hop' -> <hop>
#     'auto' -> <auto>
#     'exclude' -> [<hop>]
#     'sequence' -> [<regex>]
#     'star' -> <regex>
#     'or' -> [<regex>]

def merlin_regex_to_nodes_table(regex, node_prefix, start_node, end_node, trans):
    def hop_req_trans(hop):
        return {
            'src': lambda ip: True,
            'dst': lambda ip: True,
            'pkt': [], 'spg': [], 'spu': [], 'hop': '.',
            'req': current_hop_requirement(hop),
        }

    if regex['type'] == 'hop':
        hop = regex['value']
        if hop != '.':
            trans = merge_trans(trans, hop_req_trans(hop))
        return {
            start_node: [
                {'dest': end_node, 'trans': trans},
            ],
        }
    elif regex['type'] == 'auto':
        auto = regex['value']
        nodes_table = {}
        for node, edges in auto['nodes'].items():
            next_edges = []
            for edge in edges:
                if edge['dest'] == auto['end']:
                    if edge['trans']['hop'] == 'd':
                        raise Exception(
                            'Sub-auto of Merlin cannot contain dropping transition')
                    assert edge['trans']['hop'] == 'e'
                next_edges.append({
                    **edge, 
                    'dest': end_node, 
                    'trans': {**edge['trans'], 'hop': '.'}
                })
            nodes_table[f'{node_prefix}{node}'] = next_edges
        return {
            **nodes_table,
            start_node: [
                {'dest': f'{node_prefix}{auto["initial"]}', 'trans': 'epsilon'},
            ],
        }
    elif regex['type'] == 'exclude':
        except_item_list = regex['value']
        except_hop_list = []
        for item in except_item_list:
            if item['type'] != 'hop':
                raise Exception('Only hops can be excluded')
            except_hop_list.append(item['value'])
        dead_node = f'{node_prefix}<exclude>(dead)'
        exclude_res = Resource(0)

        def exclude_req(env):
            if env['current_hop'] in except_hop_list:
                return {exclude_res: 1}
            else:
                return {}

        return {
            start_node: [
                {
                    'dest': dead_node, 
                    'trans': trans,  # no requirement
                }  
            ] + [
                {
                    'dest': end_node, 'trans': {
                        **trans, 'req': exclude_req,
                    }
                }
            ],
        }
    elif regex['type'] == 'sequence':
        sub_regex_list = regex['value']
        sub_start_node = start_node
        nodes_table = {}
        for i, sub_regex in enumerate(sub_regex_list):
            # print(i, sub_regex)
            if i == len(sub_regex_list) - 1:
                sub_end_node = end_node
            else:
                sub_end_node = f'{node_prefix}<seq>[{i}](qe)'
            sub_nodes_table = merlin_regex_to_nodes_table(
                sub_regex, f'{node_prefix}<seq>[{i}]+', 
                sub_start_node, sub_end_node, trans,
            )
            # from pprint import pprint
            # pprint(sub_nodes_table)
            sub_start_node = sub_end_node
            assert nodes_table.keys().isdisjoint(sub_nodes_table.keys())
            nodes_table = {**nodes_table, **sub_nodes_table}
        return nodes_table
    elif regex['type'] == 'star':
        sub_regex = regex['value']
        sub_start_node, sub_end_node = \
            f'{node_prefix}<star>(q0)', f'{node_prefix}<star>(qe)'
        sub_nodes_table = merlin_regex_to_nodes_table(
            sub_regex, f'{node_prefix}<star>+',
            sub_start_node, sub_end_node, trans,
        )
        sub_nodes_table[sub_end_node] = [
            *sub_nodes_table.get(sub_end_node, []),
            {'dest': end_node, 'trans': 'epsilon'},
            {'dest': sub_start_node, 'trans': 'epsilon'},
        ]
        return {
            **sub_nodes_table,
            start_node: [
                {'dest': sub_start_node, 'trans': 'epsilon'},
                {'dest': end_node, 'trans': 'epsilon'},
            ],
        }
    elif regex['type'] == 'or':
        sub_regex_list = regex['value']
        nodes_table = {start_node: []}
        for i, sub_regex in enumerate(sub_regex_list):
            sub_start_node, sub_end_node = \
                f'{node_prefix}<or>[{i}](q0)', f'{node_prefix}<or>[{i}](qe)'
            sub_nodes_table = merlin_regex_to_nodes_table(
                sub_regex, f'{node_prefix}<or>[{i}]+', 
                sub_start_node, sub_end_node, trans
            )
            sub_nodes_table = {
                **sub_nodes_table, sub_end_node: [
                    *sub_nodes_table.get(sub_end_node, []),
                    {'dest': end_node, 'trans': 'epsilon'},
                ]
            }
            assert nodes_table.keys().isdisjoint(sub_nodes_table.keys()) 
            nodes_table = {
                **nodes_table,
                start_node: [
                    *nodes_table[start_node],
                    {'dest': sub_start_node, 'trans': 'epsilon'},
                ],
                **sub_nodes_table,
            }
        return nodes_table
    else:
        raise Exception(f'Unknown regex type {regex["type"]}')

def merlin_regex(regex_string, variables=None):
    if variables is None:
        variables = {}
    def read_one_item(regex_string):
        regex_string = regex_string.strip()
        if len(regex_string) == 0:
            raise Exception('Regex ends unexpectedly')
        # print(regex_string)
        if regex_string[0] == '(':
            try:
                range_end = regex_string.index(')')
            except ValueError:
                raise Exception('Open "(" is not closed in regex')
            sub_regex = read_all_items(regex_string[1:range_end])
        elif regex_string[0] == '[':
            try:
                range_end = regex_string.index(']')
            except ValueError:
                raise Exception('Unclosed "[" appeared in regex')
            if regex_string[1] == '^':
                range_type = 'exclude'
                range_start = 2
            else:
                range_type = 'or'
                range_start = 1
            sub_items = [
                item.strip() 
                for item in regex_string[range_start:range_end].split(',')
            ]
            sub_regex = {
                'type': range_type,
                'value': [
                    {'type': 'hop', 'value': value}
                    for value in sub_items
                ]
            }
        elif regex_string[0] == '<':
            try:
                range_end = regex_string.index('>')
            except ValueError:
                raise Exception('Unclosed "<" appeared in regex')
            name = regex_string[1:range_end]
            if len(name) == 0:
                raise Exception('Name between "<" and ">" is empty')
            if name[0] == '?':
                sub_regex = {'type': 'auto', 'value': variables[name[1:]]}
            else:
                sub_regex = {'type': 'hop', 'value': name}            
        else:
            raise Exception(f'Unexpected character "{regex_string[0]}" in regex')

        # postfix operator
        if regex_string[range_end + 1:] == '':
            return sub_regex, ''
        elif regex_string[range_end + 1] == '*':
            return {'type': 'star', 'value': sub_regex}, regex_string[range_end + 2:]
        # space around '|' is allowed
        elif regex_string[range_end + 1:].strip()[0] == '|':
            remain_regex, remain_str = \
                read_one_item(regex_string[regex_string.index('|') + 1:])
            if remain_regex['type'] == 'or':
                return {'type': 'or', 'value': [sub_regex, *remain_regex['value']]}, remain_str
            else:
                return {'type': 'or', 'value': [sub_regex, remain_regex]}, remain_str
        else:
            return sub_regex, regex_string[range_end + 1:]

    def read_all_items(regex_string):
        regex_string = regex_string.strip()
        if len(regex_string) == 0:
            raise Exception('Regex ends unexpectedly')
        # print(regex_string)
        regex, remain = read_one_item(regex_string)
        if len(remain.strip()) == 0:
            return regex
        else:
            remain_regex = read_all_items(remain)
            if remain_regex['type'] == 'sequence':
                return {'type': 'sequence', 'value': [regex, *remain_regex['value']]}
            else:
                return {'type': 'sequence', 'value': [regex, remain_regex]}

    return read_all_items(regex_string)

# snap: dict
#   type: 'id' | 'drop' | 'if'
#   value: <depends on type>
#     'id' -> None
#     'drop' -> None
#     'if' -> dict
#       guard: dict
#         src: <ip> -> bool
#         dst: <ip> -> bool
#         pkt: [<pkt>]
#         spg: [<spg>]
#       true: dict
#         spu: [<spu>]
#         expr: <snap>
#       false: <same as <true>>

def fill_default(guard):
    return {
        'src': guard.get('src', lambda ip: True),
        'dst': guard.get('dst', lambda ip: True),
        'pkt': guard.get('pkt', []), 'spg': guard.get('spg', []),
        # 'req': lambda env: {},
    }

def merge_nodes_table_with_one_shared(nt1, nt2, shared):
    assert nt1.keys() & nt2.keys() <= {shared}
    return {
        **nt1, **nt2,
        shared: [*nt1.get(shared, []), *nt2.get(shared, [])],
    }

def snap_to_nodes_table(snap, start_node, prefix, auto_end_node, inherited_guard):
    no_req = lambda env: {}
    if snap['type'] == 'id':
        assert inherited_guard['spg'] == []
        return {
            start_node: [
                {
                    'dest': start_node,
                    'trans': {
                        **inherited_guard, 'req': no_req, 'spu': [], 'hop': '.'
                    },
                }, {
                    'dest': auto_end_node,
                    'trans': {
                        **inherited_guard, 'req': no_req, 'spu': [], 'hop': 'e'
                    },
                }
            ]
        }
    elif snap['type'] == 'drop':
        assert inherited_guard['spg'] == []
        return {
            start_node: [
                {
                    'dest': start_node,
                    'trans': {
                        **inherited_guard, 'req': no_req, 'spu': [], 'hop': '.'
                    },
                }, {
                    'dest': auto_end_node,
                    'trans': {
                        **inherited_guard, 'req': no_req, 'spu': [], 'hop': 'd'
                    },
                }
            ]
        }
    elif snap['type'] == 'if':
        guard, true_u, true_e, false_u, false_e = \
            snap['value']['guard'], snap['value']['true'].get('spu', []), \
            snap['value']['true']['expr'], \
            snap['value']['false'].get('spu', []), \
            snap['value']['false']['expr']

        full_guard = fill_default(guard)
        neg_guard_list = [
            fill_default({'src': lambda ip: not full_guard['src'](ip)}), 
            fill_default({'dst': lambda ip: not full_guard['dst'](ip)}),
            *[
                fill_default({'pkt': [negate_comp(pkt)]}) 
                for pkt in full_guard['pkt']
            ], *[
                fill_default({'spg': [negate_comp(spg)]}) 
                for spg in full_guard['spg']
            ],
        ]
        def mix_guard(g1, g2):
            mixed_with_update = merge_trans(
                {**g1, 'spu': [], 'hop': '.', 'req': no_req}, 
                {**g2, 'spu': [], 'hop': '.', 'req': no_req},
            )
            return {
                key: mixed_with_update[key]
                for key in ['src', 'dst', 'pkt', 'spg']
            }
        mixed_true_guard = mix_guard(inherited_guard, full_guard)
        nodes_table = {
            start_node: [
                # do nothing and stay here
                # make sure packet with every kind of `pkt` can stay here
                {
                    'dest': start_node, 'trans': {
                        **fill_default({'pkt': pkt}), 
                        'hop': '.', 'req': no_req, 'spu': [],
                    }
                } for pkt in [full_guard['pkt']] + \
                    [[negate_comp(pkt)] for pkt in full_guard['pkt']]
            ]
        }
        if len(true_u) != 0 or true_e['type'] != 'if':
            true_nodes_table = guard_nodes_table(
                mixed_true_guard, true_u, true_e, start_node, auto_end_node, 
                f'{prefix}<true>')
            # from pprint import pprint
            # pprint(nodes_table)
        else:
            true_nodes_table = snap_to_nodes_table(
                true_e, start_node, f'{prefix}<true>(pass down)', auto_end_node, 
                mixed_true_guard)
        nodes_table = merge_nodes_table_with_one_shared(
            nodes_table, true_nodes_table, start_node)
        for i, neg_guard in enumerate(neg_guard_list):
            mixed_false_guard = mix_guard(inherited_guard, neg_guard)
            if len(false_u) != 0 or false_e['type'] != 'if':
                neg_nodes_table = guard_nodes_table(
                    mixed_false_guard, false_u, false_e, start_node, auto_end_node, 
                    f'{prefix}<false>[{i}]')
            else:
                neg_nodes_table = snap_to_nodes_table(
                    false_e, start_node, f'{prefix}<false>[{i}](pass down)',
                    auto_end_node, mixed_false_guard)
            assert nodes_table.keys() & neg_nodes_table.keys() <= {start_node}
            nodes_table = merge_nodes_table_with_one_shared(
                nodes_table, neg_nodes_table, start_node)
        return nodes_table
    else:
        raise Exception(f'Unknown SNAP expression type {snap["type"]}')

def guard_nodes_table(guard, update, expr, start_node, auto_end_node, prefix):
    no_req = lambda env: {}
    if len(guard['spg']) == 0 and len(update) == 0:
        # only expr is left, do it and reach destination
        return snap_to_nodes_table(
            expr, start_node, f'{prefix}<expr>', auto_end_node, guard)
    else:
        nodes_table = {start_node: []}
        # print(guard, update)
        for i in range(len(guard['spg'])):
            spg_here, spg_left = guard['spg'][:i + 1], guard['spg'][i + 1:]
            after_guard_node = f'{prefix}<guard>[{i}](after)'
            next_nodes_table = guard_nodes_table(
                {**guard, 'spg': spg_left}, update, expr, after_guard_node, 
                auto_end_node, f'{prefix}<guard>[{i}]+')
            assert start_node not in next_nodes_table
            sub_nodes_table = {
                **next_nodes_table,
                start_node: [
                    {
                        'dest': after_guard_node, 'trans': {
                            **guard, 
                            'spg': spg_here, 'spu': [], 'hop': '.', 
                            'req': no_req,
                        },
                    },
                ]
            }
            assert nodes_table.keys() & sub_nodes_table.keys() == {start_node}
            nodes_table = merge_nodes_table_with_one_shared(
                nodes_table, sub_nodes_table, start_node)
        for i in range(len(update)):
            update_here, update_left = update[:i + 1], update[i + 1:]
            after_update_node = f'{prefix}<update>[{i}](after)'
            next_nodes_table = guard_nodes_table(
                {**guard, 'spg': []}, update_left, expr, after_update_node, 
                auto_end_node, f'{prefix}<update>[{i}]+')
            assert start_node not in next_nodes_table
            sub_nodes_table = {
                **next_nodes_table,
                start_node: [
                    {
                        'dest': after_update_node, 'trans': {
                            **guard, 
                            'spu': update_here, 'hop': '.', 'req': no_req,
                        },
                    },
                ]
            }
            assert nodes_table.keys() & sub_nodes_table.keys() == {start_node}
            nodes_table = merge_nodes_table_with_one_shared(
                nodes_table, sub_nodes_table, start_node)
        if expr['type'] == 'id':
            # do all guards & updates, and reach destination directly
            nodes_table[start_node].append({
                'dest': auto_end_node, 'trans': {
                    **guard, 'spu': update, 'hop': 'e', 'req': no_req,
                }
            })
        elif expr['type'] == 'drop':
            # print(guard)
            # do all guards & updates, and drop
            nodes_table[start_node].append({
                'dest': auto_end_node, 'trans': {
                    **guard, 'spu': update, 'hop': 'd', 'req': no_req,
                }
            })
        return nodes_table

def snap(snap_table):
    nodes_table = snap_to_nodes_table(
        snap_table, '<snap>(q0)', '<snap>+', '<snap>(qe)', fill_default({}))
    # from pprint import pprint
    # pprint(nodes_table)
    
    return determine_auto({
        'initial': '<snap>(q0)', 'end': '<snap>(qe)',
        'nodes': nodes_table,
    })
