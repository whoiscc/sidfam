# sidfam/test.py

from unittest import main


def load_tests(loader, tests, pattern):
    return loader.discover('test', pattern='*.py')

if __name__ == '__main__':
    main()
