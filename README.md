# Sidfam Project

[![pipeline status](https://gitlab.com/whoiscc/sidfam/badges/master/pipeline.svg)](https://gitlab.com/whoiscc/sidfam/commits/master)
[![coverage report](https://gitlab.com/whoiscc/sidfam/badges/master/coverage.svg)](https://gitlab.com/whoiscc/sidfam/commits/master)

Sidfam is a vanilla implementation of CODER, which is a set of IR & compiler for software defined networking.

The `solve_auto` function in `main.py` accepts a CODER stateful automaton, along with a network topography, and returns rules generated for that automaton. The structure of automaton, network and rule can be found in `main.py`. You may also need to use `Resource` class to define resource requirement in automaton.

The default solver of MIL problems is [PuLP][1]. If you want to use custom solver, port it follow the interfaces defined in `prob.py`. The custom solver could be a optional argument passed into `solve_auto`, as well as be used to replace default solver globally. It is recommend to modify `prob.py` to support more solvers out of box, and make a PR to this project.

Compiler frontends port to [Merlin][2] and [SNAP][3] can be found in `lang.py`. Several operators of automata are defined in `oper.py`. You could extend them as you need.

If you want to make experiments on the implementation, or enhance this project, feel free to run `coverage run test.py` to make sure all test cases are passed, and all code is tested.

## What to Do Next

* Clean up `core.py`. Move helper functions to `util.py`.
* Redesign interface of `build_problem_path` to keep it uniform with other `build_problem_*` functions.
* Replace common static tiny components, like `no_req` and `merge_req`, with functions live in `util.py`.
* Replace common algorithm skills, like group items in a dict into several small dicts, with Python's standard library utilities or custom defined functions.
* Format comments.
* Write more test cases for most functions, which only has a poor `test_simple` currently.
* Wrap some functions into class-based APIs. These functions have an OOP nature, such as merlin's frontend. Put refined APIs into `apis.py`.
* Raise custom exceptions instead of `Exception`.
* Add support for more MILP solvers, more operators, and more SDN language.

----

Copyright &copy; 2018 Correctizer.

[1]: https://pythonhosted.org/PuLP/
[2]: http://www.cs.cornell.edu/~jnfoster/papers/merlin.pdf
[3]: https://www.cs.princeton.edu/~jrex/papers/snap16.pdf
