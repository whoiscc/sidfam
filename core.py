# sidfam/core.py -- Core code of project Sidfam.
# Created by Correctizer on Jul 7, 2018.

from prob import create_problem, create_variable, constraint_problem_le, \
    constraint_problem_ge, constraint_problem_eq, solve_problem

# trans: dict
#   src: ip -> bool
#   dst: ip -> bool
#   pkt: [<pkt>]
#   spg: [<spg>]
#   req: <req>
#   spu: [<spu>]
#   hop: <hop>

# sdt (trans with source and destination): dict
#   src: ip
#   dst: ip
#   <rest part same as trans>

def trans_to_sdt_generator(trans, ip_list):
    for src_ip in filter(trans['src'], ip_list):
        for dst_ip in filter(trans['dst'], ip_list):
            if src_ip == dst_ip:
                continue
            yield {
                **trans,
                'src': src_ip, 'dst': dst_ip,
            }

# auto: dict
#   initial: <node>
#   end: <node>
#   nodes: <nodes>
# nodes: dict
#   <node>: [<edge>]
# edge: dict
#   trans: <trans> | "epsilon"
#   dest: <node>

def closure(auto, node_set):
    assert type(node_set) is set
    closure_set = set(node_set)
    uncovered_set = set(node_set)
    while True:
        next_uncovered_set = set()
        for node in uncovered_set:
            if node not in auto['nodes']:
                continue
            for edge in auto['nodes'][node]:
                if edge['trans'] == 'epsilon' and \
                        edge['dest'] not in closure_set:
                    next_uncovered_set.add(edge['dest'])
        closure_set |= next_uncovered_set
        uncovered_set = next_uncovered_set
        if len(uncovered_set) == 0:
            break
    return closure_set

def goto_generator(auto, node_set):
    goto_set = set()
    for node in node_set:
        if node not in auto['nodes']:
            continue
        for edge in auto['nodes'][node]:
            if edge['trans'] == 'epsilon':
                continue
            yield edge['trans'], closure(auto, set([edge['dest']]))

def determine_auto(auto):
    next_initial = frozenset(closure(auto, {auto['initial']}))
    node_set_cat = {next_initial: []}
    uncovered_set_cat = {next_initial}
    while True:
        next_uncovered_set_cat = set()
        for node_set in uncovered_set_cat:
            for trans, dest in goto_generator(auto, node_set):
                dest_node_set = frozenset(dest)
                if dest_node_set not in node_set_cat:
                    next_uncovered_set_cat.add(dest_node_set)
                node_set_cat[node_set].append({
                    'trans': trans,
                    'dest': dest_node_set,
                })
        for node_set in next_uncovered_set_cat:
            node_set_cat[node_set] = []
        uncovered_set_cat = next_uncovered_set_cat
        if len(uncovered_set_cat) == 0:
            break
    
    node_count = 0
    node_name = {}
    for node_set in node_set_cat:
        if auto['end'] not in node_set:
            node_name[node_set] = f'q{node_count}'
            node_count += 1
        else:
            node_name[node_set] = 'qe'
    
    next_nodes = {}
    for node_set in node_set_cat:
        for edge in node_set_cat[node_set]:
            if node_name[node_set] not in next_nodes:
                next_nodes[node_name[node_set]] = []
            if node_name[edge['dest']] == 'qe':
                # print(edge)
                assert edge['trans']['hop'] in ['e', 'd']
            next_nodes[node_name[node_set]].append({
                'trans': edge['trans'],
                'dest': node_name[edge['dest']],
            })

    return {
        'initial': node_name[next_initial],
        'end': 'qe',
        'nodes': next_nodes
    }

# ia (inflated auto): dict
#   edge: dict
#     sdt: <sdt>
#     dest: <node>
# <other same as auto>

def inflate_auto(auto, ip_list, hop_list):
    # dot_range[<node>] = {<hop - '.'>}
    dot_range = {}
    for node in auto['nodes']:
        hop_set = set()
        for edge in auto['nodes'][node]:
            if edge['trans']['hop'] != '.':
                hop_set.add(edge['trans']['hop'])
        assert (hop_set - {'e', 'd'}) <= set(hop_list)
        dot_range[node] = set(hop_list) - hop_set

    next_nodes = {}
    for node in auto['nodes']:
        next_nodes[node] = []
        for edge in auto['nodes'][node]:
            for sdt in trans_to_sdt_generator(edge['trans'], ip_list):
                if sdt['hop'] == '.':
                    available_hop = dot_range[node]
                else:
                    available_hop = {sdt['hop']}
                for hop in available_hop:
                    next_nodes[node].append({
                        'sdt': {**sdt, 'hop': hop},
                        'dest': edge['dest'],
                    })
    
    return {
        **auto,
        'nodes': next_nodes,
    }

# comp (pkt, spg, spu): dict
#   dep: {<dep>}
#   equ: (<$self>, <$self>) -> bool
# <spu.equ> and <pkt.dep> is needless

def is_equal_comp(comp1, comp2):
    return comp1['equ'](comp1, comp2) and comp2['equ'](comp1, comp2)

def contains_all(cl1, cl2):
    for comp1 in cl1:
        contained = False
        for comp2 in cl2:
            if is_equal_comp(comp1, comp2):
                contained = True
        if not contained:
            return False
    return True

def same_comp_list(cl1, cl2):
    return contains_all(cl1, cl2) and contains_all(cl2, cl1)

def same_edge_group(edge1, edge2):
    sdt1, sdt2 = edge1['sdt'], edge2['sdt']
    return sdt1['src'] == sdt2['src'] and \
        sdt1['dst'] == sdt2['dst'] and \
        same_comp_list(sdt1['pkt'], sdt2['pkt']) and \
        same_comp_list(sdt1['spg'], sdt2['spg'])

# edge2['spg'] <= edge1['spg']
def compitable_edge_group(edge1, edge2):
    sdt1, sdt2 = edge1['sdt'], edge2['sdt']
    return sdt1['src'] == sdt2['src'] and \
        sdt1['dst'] == sdt2['dst'] and \
        same_comp_list(sdt1['pkt'], sdt2['pkt']) and \
        contains_all(sdt2['spg'], sdt1['spg'])


def merge_comp_list(cl1, cl2):
    merged_list = list(cl1)
    for comp2 in cl2:
        contained = False
        for comp1 in cl1:
            if is_equal_comp(comp1, comp2):
                contained = True
                break
        if not contained:
            merged_list.append(comp2)
    return merged_list

# ck: dict
#   src: <ip>
#   dst: <ip>
#   pkt: [<pkt>]

# `ck1` could be a <ck> or a <sdt>
def compatible_choice_key(ck1, ck2):
    if ck2 is None:
        return True
    return ck1['src'] == ck2['src'] and ck1['dst'] == ck2['dst']

def merge_choice_key(ck1, ck2):
    if ck2 is None:
        return {
            'src': ck1['src'], 'dst': ck1['dst'], 'pkt': ck1['pkt'],
        }
    assert compatible_choice_key(ck1, ck2)
    return {
        'src': ck1['src'],
        'dst': ck1['dst'],
        'pkt': merge_comp_list(ck1['pkt'], ck2['pkt']),
    }

# fia (fixed inflated auto): dict
#   src: <ip>
#   dst: <ip>
#   ia: <ia>

# group: dict
#   <node>: [<ce>]
# ce: dict
#   edges: [<edge>]
#   ck: <ck>

def search_node(group, node, end_node, visited, choice_key):
    # print(node)
    if node == end_node:
        yield choice_key, {node: -1}
        return
    if node not in group:  # no leaving edge
        return
    next_visited = visited | set([node])
    for i, ce in enumerate(group[node]):
        if not compatible_choice_key(ce['ck'], choice_key):
            continue
        for ck, choice in search_edge_group(
                group, ce, end_node, next_visited, 
                merge_choice_key(ce['ck'], choice_key)):
            yield ck, {**choice, node: i}

# I feel regret to write this algorithm,
# which would waste infinite computing time of CPUs in this world.
def merge_choice_list(cl1, cl2):
    if len(cl1) == 0:
        return cl2
    if len(cl2) == 0:
        return cl1
    merged1, merged2 = [False] * len(cl1), [False] * len(cl2)
    merged_list = []
    for i1, (ck1, choice1) in enumerate(cl1):
        for i2, (ck2, choice2) in enumerate(cl2):
            assert compatible_choice_key(ck1, ck2)
            conflict = False
            for node in choice1.keys() & choice2.keys():
                if choice1[node] != choice2[node]:
                    conflict = True
                    break
            if not conflict:
                merged_list.append(
                    (merge_choice_key(ck1, ck2), {**choice1, **choice2})
                )
                merged1[i1] = merged2[i2] = True
    assert all(merged1 + merged2)
    return merged_list

def search_edge_group(group, ce, end_node, visited, choice_key):
    dest_list = set([edge['dest'] for edge in ce['edges']]) - visited
    # print(f'dest_list: {dest_list}')
    choice_list = None
    for dest in dest_list:
        dest_choice_list = [
            (ck, choice)
            for ck, choice in
                    search_node(group, dest, end_node, visited, choice_key)
            if compatible_choice_key(ck, choice_key)        
        ]
        if choice_list is None:
            choice_list = dest_choice_list
        else:
            choice_list = merge_choice_list(choice_list, dest_choice_list)
    if choice_list is None:
        choice_list = []  # no choice
    for ck, choice in choice_list:
        if compatible_choice_key(ce['ck'], ck) and end_node in choice:
            yield merge_choice_key(ce['ck'], ck), choice

def build_fia(ia, group, choice_key, choice):
    # print(choice)
    nodes = {}
    for node in choice:
        if node == ia['end']:
            continue
        assert choice[node] < len(group[node])
        ce = group[node][choice[node]]
        assert compatible_choice_key(ce['ck'], choice_key)
        nodes[node] = []
        for edge in ce['edges']:
            sdt = edge['sdt']
            assert sdt['src'] == choice_key['src']
            assert sdt['dst'] == choice_key['dst']
            nodes[node].append({
                'dest': edge['dest'],
                'sdt': {
                    **sdt,
                    'src': choice_key['src'],
                    'dst': choice_key['dst'],
                    'pkt': choice_key['pkt'],
                },
            })
    return {
        'src': choice_key['src'],
        'dst': choice_key['dst'],
        'ia': {**ia, 'nodes': nodes},
    }

def split_ia(ia):
    group = {}
    for node in ia['nodes']:
        group[node] = []
        # insert loop #1, create all edge groups
        inserted = {}
        for i, edge in enumerate(ia['nodes'][node]):
            # spg is not transport to other edges
            # spg_list = edge['sdt']['spg']
            # self_conflict = False
            # for spg1 in spg_list:
            #     if 'neg' in spg1 and any(
            #             [is_equal_comp(spg1['neg'], spg2) for spg2 in spg_list]):
            #         self_conflict = True
            #         break
            # if self_conflict:
            #     inserted[i] = -1  # drop
            #     continue    

            for j, edge_group in enumerate(group[node]):
                if same_edge_group(edge_group['edges'][0], edge):
                    edge_group['edges'].append(edge)
                    inserted[i] = j
                    break
            if i not in inserted:
                group[node].append({
                    'edges': [edge],
                    'ck': {
                        'src': edge['sdt']['src'], 
                        'dst': edge['sdt']['dst'],
                        'pkt': edge['sdt']['pkt'],
                    }
                })
                inserted[i] = len(group[node]) - 1
        # insert loop #2, insert missing compatible edges
        for i, edge in enumerate(ia['nodes'][node]):
            # if inserted[i] == -1:
            #     continue
            for j, edge_group in enumerate(group[node]):
                assert i in inserted
                if inserted[i] != j and \
                        compitable_edge_group(edge_group['edges'][0], edge):
                    edge_group['edges'].append(edge)

        # remove more general edge groups, if more strict version exists
        removed_groups = set()
        for i, edge_group1 in enumerate(group[node]):
            if i in removed_groups:
                continue
            for j, edge_group2 in enumerate(group[node]):
                if j in removed_groups or i == j:
                    continue
                if compitable_edge_group(
                        edge_group1['edges'][0], edge_group2['edges'][0]):
                    removed_groups.add(j)
        group[node] = [
            edge_group for i, edge_group in enumerate(group[node]) 
            if i not in removed_groups
        ]

    for choice_key, choice in search_node(
            group, ia['initial'], ia['end'], set(), None):         
        pkt_list = choice_key['pkt']
        self_conflict = False
        for pkt1 in pkt_list:
            if 'neg' in pkt1 and any(
                    [is_equal_comp(pkt1['neg'], pkt2) for pkt2 in pkt_list]):
                self_conflict = True
                break
        if self_conflict:
            continue    
        yield build_fia(ia, group, choice_key, choice)

# pg (path graph): dict
#   vert: <initial vertex> + [<vertex>]
#   edge: [(<index of <vert>>, <index of <vert>>)]
# initial vertex: dict
#   next_hop: <hop>
# vertex: dict
#   src: <ip>
#   dst: <ip>
#   pkt: [<pkt>]
#   spg: [<spg>]
#   req: <req>
#   spu: [<spu>]
#   next_hop: <hop>
# nw (network): dict
#   ip: dict
#     <ip>: <hop>
#   conn: dict
#     <hop>: [<hop>]

def edge_to_vertex(edge):
    return {
        **{
            key: edge['sdt'][key] 
            for key in ['src', 'dst', 'pkt', 'spg', 'req', 'spu']
        },
        'next_hop': edge['sdt']['hop'],
    }

def convert_fia_to_path_graph(fia, network):
    from pprint import pprint
    # pprint(fia)
    # node_vertex[<node>] = [<index of <pg.vert>>]
    node_vertex = {fia['ia']['initial']: [0]}
    # edge_vertex[<node>, <index of edge>] = <index of <pg.vert>>
    edge_vertex = {}
    vert_list = [{'next_hop': network['ip'][fia['src']]}]
    for node in fia['ia']['nodes']:
        for i, edge in enumerate(fia['ia']['nodes'][node]):
            if edge['dest'] == fia['ia']['end']:
                assert edge['sdt']['hop'] in ['e', 'd']
            vert_index = len(vert_list)
            vert_list.append(edge_to_vertex(edge))
            if edge['dest'] not in node_vertex:
                node_vertex[edge['dest']] = []
            node_vertex[edge['dest']].append(vert_index)
            edge_vertex[node, i] = vert_index
    # print(node_vertex)
    edge_list = []
    for node in fia['ia']['nodes']:
        for i, edge in enumerate(fia['ia']['nodes'][node]):
            dest_vert_index = edge_vertex[node, i]
            dest_vert = vert_list[dest_vert_index]
            for vert_index in node_vertex[node]:
                vert = vert_list[vert_index]
                if dest_vert['next_hop'] == 'e':
                    # print(node, edge)
                    # print(vert['next_hop'])
                    reachable = vert['next_hop'] == network['ip'][fia['dst']]
                elif dest_vert['next_hop'] == 'd':
                    reachable = True  # you can drop a packet everywhere
                else:
                    reachable = \
                        dest_vert['next_hop'] in \
                        network['conn'][vert['next_hop']]
                if reachable:
                    edge_list.append((vert_index, dest_vert_index))
    return {
        'vert': vert_list,
        'edge': edge_list,
    }

def comp_list_deps(comp_list):
    deps = set()
    for comp in comp_list:
        deps |= comp['dep']
    return deps

def vertex_deps(vertex):
    return comp_list_deps(vertex['spg']) | \
        comp_list_deps(vertex['spu'])

# pgpv (path graph & problem with variables): dict
#   pg: <pg>
#   ev: dict
#     <edge>: <var>
#   pb: <prob>

def build_problem_path(prob, pg):
    edge_var = {edge: create_variable(prob) for edge in pg['edge']}
    enter_hop, leave_hop = {}, {}
    enter_vertex, leave_vertex = {}, {}  # use index as key
    leave_src_vertex, enter_dest_vertex = [], []
    src_hop, dest_hop, drop_hop_list = pg['vert'][0]['next_hop'], None, []
    # print(pg['vert'])
    for edge in pg['edge']:
        from_vert, to_vert = edge
        from_hop, to_hop = \
            pg['vert'][from_vert]['next_hop'], pg['vert'][to_vert]['next_hop']
        
        if from_hop not in leave_hop:
            leave_hop[from_hop] = []
        leave_hop[from_hop].append(edge)
        if to_hop not in enter_hop:
            enter_hop[to_hop] = []
        enter_hop[to_hop].append(edge)

        if from_vert not in leave_vertex:
            leave_vertex[from_vert] = []
        leave_vertex[from_vert].append(edge)
        if to_vert not in enter_vertex:
            enter_vertex[to_vert] = []
        enter_vertex[to_vert].append(edge)

        if edge[0] == 0:
            leave_src_vertex.append(edge)
        if to_hop == 'e':
            # dest hop must be unique...
            if dest_hop is None:
                dest_hop = from_hop
            else:
                assert from_hop == dest_hop
            enter_dest_vertex.append(edge)
        if to_hop == 'd':
            # ... but drop hops could be a lot
            drop_hop_list.append(to_hop)
            enter_dest_vertex.append(edge)
    # There could be not dest hop, but a few drop hops instead
    # assert dest_hop is not None
    if drop_hop_list + [dest_hop] == []:
        raise Exception('There is no available exit hop in network')

    for hop in (enter_hop.keys() - {'e', 'd'}) | leave_hop.keys():
        if hop in enter_hop:
            enter_expr = {edge_var[edge]: 1 for edge in enter_hop[hop]}
        else:
            enter_expr = {}
        if hop in leave_hop:
            leave_expr = {edge_var[edge]: -1 for edge in leave_hop[hop]}
        else:
            leave_expr = {}
        assert enter_expr.keys().isdisjoint(leave_expr.keys())
        if hop == src_hop:
            # sum(<enter edges>) == 0
            constraint_problem_eq(prob, enter_expr, 0)
            # sum(<leave edges>) == 1 (to normal hop, or to 'd')
            if leave_expr == {}:
                raise Exception('No available way to leave initial hop')
            constraint_problem_eq(prob, leave_expr, -1)
        else:
            # sum(<enter edges>) == sum(<leave edges>) <= 1
            # For drop hops, one way in and one way out to 'd',
            # for dest hop, one way in and one way out to 'e',
            # and packet may be dropped before reaching, so they obey this rule.
            constraint_problem_le(prob, enter_expr, 1)
            # constraint_problem_ge(prob, leave_expr, -1)
            if enter_expr != {} or leave_expr != {}:
                constraint_problem_eq(prob, {**enter_expr, **leave_expr}, 0)
    
    if leave_src_vertex == []:
        raise Exception('No available way to leave initial state')
    constraint_problem_eq(
        prob, {edge_var[edge]: 1 for edge in leave_src_vertex}, 1,
    )
    if enter_dest_vertex == []:
        raise Exception('No available way to enter destination state')
    constraint_problem_eq(
        prob, {edge_var[edge]: 1 for edge in enter_dest_vertex}, 1,
    )

    for vert_index in range(len(pg['vert']))[1:]:
        if pg['vert'][vert_index]['next_hop'] in ['e', 'd']:
            continue
        if vert_index in enter_vertex:
            # sum(<enter edges>) <= 1
            enter_expr = {edge_var[edge]: 1 for edge in enter_vertex[vert_index]}
            constraint_problem_le(prob, enter_expr, 1)
        else:
            enter_expr = {}
        if vert_index in leave_vertex:
            # sum(<leave edges>) <= 1
            leave_expr = {edge_var[edge]: -1 for edge in leave_vertex[vert_index]}
            constraint_problem_ge(prob, leave_expr, -1)
        else:
            leave_expr = {}
        # There could be vertex in path graph, which does not connect to any
        # other vertices.
        # They are reachable in automaton, but all the vertices of the previous
        # states live on the hops that does not connect to this vertex's hop.
        # assert len(enter_expr) + len(leave_expr) > 0
        assert enter_expr.keys().isdisjoint(leave_expr.keys())
        # sum(<enter edges>) == sum(<leave edges>)
        if enter_expr != {} or leave_expr != {}:
            constraint_problem_eq(prob, {**enter_expr, **leave_expr}, 0)
    
    return {
        'pg': pg, 'pb': prob, 'ev': edge_var
    }

def build_problem_hck(pgpv_list):
    # Packets may have the same choice key, even if they are in different path
    # graph of same problem.
    # Packets with same choice key cannot flow into same hop in their own paths,
    # unless sp guards they do there are different.
    # Or the hop cannot determine them with choice key. Rule conflict happens
    # this way.
    # On the other hand, if these two kinds of packets are sending to the same
    # hop from here, including 'e' and 'd', then there's no need to determine
    # them from each other here. So that is allowed.
    prob = None
    # same_ck_spg[<hop>] = 
    #     [[(<edges's pg>, <edges with same choice key and spg>, <edges' var>)]]
    same_ck_spg = {}
    for pgpv in pgpv_list:
        pb, pg, edge_var = pgpv['pb'], pgpv['pg'], pgpv['ev']
        if prob is None:
            prob = pb
        else:
            assert pb is prob
        for edge, var in edge_var.items():
            hop = pg['vert'][edge[0]]['next_hop']
            # use original vertex is ok
            ck_spg = {
                key: pg['vert'][edge[1]][key] 
                for key in ['src', 'dst', 'pkt', 'spg']
            }
            if hop not in same_ck_spg:
                same_ck_spg[hop] = []
            inserted = False
            for group in same_ck_spg[hop]:
                sample_pg, sample_edge, _ = group[0]
                # print(sample_edge)
                assert sample_pg['vert'][sample_edge[0]]['next_hop'] == hop
                sample = sample_pg['vert'][sample_edge[1]]
                if ck_spg['src'] == sample['src'] and \
                        ck_spg['dst'] == sample['dst'] and \
                        same_comp_list(ck_spg['pkt'], sample['pkt']) and \
                        same_comp_list(ck_spg['spg'], sample['spg']):
                    group.append((pg, edge, var))
                    inserted = True
            if not inserted:
                same_ck_spg[hop].append([(pg, edge, var)])
    from pprint import pprint
    # pprint(same_ck_spg)

    for hop, group_list in same_ck_spg.items():
        for group in group_list:
            next_hop_var = {}
            for pg, edge, var in group:
                next_hop = pg['vert'][edge[1]]['next_hop']
                if next_hop not in next_hop_var:
                    next_hop_var[next_hop] = create_variable(prob)
                # edge can only be choosed when next hop is choosed
                # edge var <= next hop var
                constraint_problem_le(
                    prob, {var: 1, next_hop_var[next_hop]: -1}, 0)
            # only one hop can be choosed
            # sum(<next hop var>) <= 1
            constraint_problem_le(
                prob, {var: 1 for var in next_hop_var.values()}, 1)

def build_problem_deps(pgpv_list):
    prob = None
    # deps_table[<dep>][<hop>] = [<var>]
    deps_table = {}
    for pgpv in pgpv_list:
        pb, pg, edge_var = pgpv['pb'], pgpv['pg'], pgpv['ev']
        if prob is None:
            prob = pb
        else:
            assert pb is prob
        for edge, var in edge_var.items():
            for dep in vertex_deps(pg['vert'][edge[1]]):
                # print(var, pg['vert'][edge[0]])
                if dep not in deps_table:
                    deps_table[dep] = {}
                current_hop = pg['vert'][edge[0]]['next_hop']
                if current_hop not in deps_table[dep]:
                    deps_table[dep][current_hop] = []
                deps_table[dep][current_hop].append(var)
    
    # print(deps_table)
    assert prob is not None
    for dep, hop_var in deps_table.items():
        choose_var = {}
        for hop, var_list in hop_var.items():
            choose_var[hop] = create_variable(prob)
            for var in var_list:
                # <var> <= choose_var[hop]
                constraint_problem_le(prob, {choose_var[hop]: -1, var: 1}, 0)
        # sum(<choose_var>) <= 1
        # print(choose_var.values())
        constraint_problem_le(prob, {var: 1 for var in choose_var.values()}, 1)

# rule: dict
#   <hop>: [<ma>]
# ma: dict
#   match: dict
#     src: ip
#     dst: ip
#     pkt: [<pkt>]
#     spg: [<spg>]
#   action: dict
#     spu: [<spu>]
#     next_hop: <hop>

def convert_solution_to_rule(sol, pgpv_list):
    rule = {}
    prob = None
    for pgpv in pgpv_list:
        pb, pg, ev = pgpv['pb'], pgpv['pg'], pgpv['ev']
        # print(ev)
        if prob is None:
            prob = pb
        else:
            assert pb is prob
        for edge, var in ev.items():
            # print(var)
            if sol[var]:
                vert_src, vert_dst = pg['vert'][edge[0]], pg['vert'][edge[1]]
                current_hop = vert_src['next_hop']
                if current_hop not in rule:
                    rule[current_hop] = []
                ma = {
                    'match': {
                        key: vert_dst[key] for key in ['src', 'dst', 'pkt', 'spg']
                    },
                    'action': {
                        key: vert_dst[key] for key in ['spu', 'next_hop']
                    },
                }
                contained = False
                for exist_ma in rule[current_hop]:
                    if ma['match'] == exist_ma['match']:
                        if ma['action'] == exist_ma['action']:
                            contained = True
                            break
                        else:
                            print(f'warning: rules conflict on hop {current_hop}')
                if not contained:
                    rule[current_hop].append(ma)
    return rule

# req: (<node>, <node>) -> dict
#   <res>: <factor>
# res: Resource

class Resource:
    def __init__(self, ration = 0, shared=False):
        self._ration = ration
        self._shared = shared

    def set_ration(self, ration):
        self._ration = ration

    def get_ration(self):
        return self._ration

    def set_shared(self, shared):
        self._shared = shared
    
    def get_shared(self):
        return self._shared

def edge_requirement(pg, edge):
    v1, v2 = pg['vert'][edge[0]], pg['vert'][edge[1]]
    current_hop, next_hop = v1['next_hop'], v2['next_hop']
    assert 'src' not in v1 or v1['src'] == v2['src']
    assert 'dst' not in v1 or v1['dst'] == v2['dst']
    assert 'pkt' not in v1 or v1['pkt'] == v2['pkt']
    src, dst, pkt, spg, spu = v2['src'], v2['dst'], v2['pkt'], v2['spg'], v2['spu']
    deps = vertex_deps(v2)
    return v2['req']({
        'current_hop': current_hop, 'next_hop': next_hop,
        'src': src, 'dst': dst, 'pkt': pkt, 'deps': deps,
        'spg': spg, 'spu': spu,
    })

def build_problem_req(pgpv_list):
    prob = None
    # res_table[<res>] = {<var>: (<factor>, (<src>, <dst>))}
    res_table = {}
    for pgpv in pgpv_list:
        pb, pg, ev = pgpv['pb'], pgpv['pg'], pgpv['ev']
        if prob is None:
            prob = pb
        else:
            assert pb is prob

        for edge, var in ev.items():
            # print(pg['vert'][edge[0]]['next_hop'], pg['vert'][edge[1]]['next_hop'])
            edge_req = edge_requirement(pg, edge)
            # print(edge_req)
            for res, req in edge_req.items():
                if res not in res_table:
                    res_table[res] = {}
                dest_vertex = pg['vert'][edge[1]]  # src may be entry point
                res_table[res][var] = req, (dest_vertex['src'], dest_vertex['dst'])
    
    assert prob is not None
    # from pprint import pprint
    # pprint(res_table)
    for res, req in res_table.items():
        if not res.get_shared():
            constraint_problem_le(
                prob, 
                {var: factor for var, (factor, _) in req.items()},
                res.get_ration(),
            )
        else:
            # group variables by edge's src/dst IP
            same_src_dst = {}  # same_src_dst[<src>, <dst>] = ([<var>], <factor>)
            for var, (factor, src_dst) in req.items():
                if src_dst not in same_src_dst:
                    same_src_dst[src_dst] = [], 0
                same_src_dst[src_dst] = (
                    same_src_dst[src_dst][0] + [var],
                    max(same_src_dst[src_dst][1], factor),
                )
            # group variable: if any of edges in this group is chosen
            gv_list = {}
            for group in same_src_dst:
                gv = create_variable(prob)
                for var in same_src_dst[group][0]:
                    # var <= group variable
                    constraint_problem_le(prob, {var: 1, gv: -1}, 0)
                gv_list[group] = gv
            # sum(<group variable> * <factor>) <= <ration>
            constraint_problem_le(
                prob,
                {var: same_src_dst[group][1] for group, var in gv_list.items()},
                res.get_ration(),
            )

def solve_auto(auto, network, solver=None):
    ip_list = list(network['ip'].keys())
    hop_list = list(network['conn'].keys())

    da = determine_auto(auto)
    ia = inflate_auto(auto, ip_list, hop_list)
    fia_list = split_ia(ia)

    prob = create_problem(solver=solver)
    pg_list = [convert_fia_to_path_graph(fia, network) for fia in fia_list]
    # print(f'fia_list length: {len(list(pg_list))}')
    # from pprint import pprint
    # pprint(pg_list)
    # print('after path graph')
    pgpv_list = [build_problem_path(prob, pg) for pg in pg_list]
    # print('after path')
    build_problem_deps(pgpv_list)
    # print('after deps')
    build_problem_req(pgpv_list)
    # print('after req')
    build_problem_hck(pgpv_list)
    # print('after hck')

    sol = solve_problem(prob)
    rule = convert_solution_to_rule(sol, pgpv_list)
    return rule

def merge_hop(hop1, hop2):
    if hop1 == hop2:
        return hop1
    elif hop1 == '.' and hop2 not in ['e', 'd']:
        return hop2
    elif hop2 == '.' and hop1 not in ['e', 'd']:
        return hop1
    else:
        return None

def merge_trans(trans1, trans2):
    def merged_req(env):
        req1, req2 = trans1['req'](env), trans2['req'](env)
        return {
            res: req1.get(res, 0) + req2.get(res, 0) 
            for res in req1.keys() | req2.keys()
        }

    hop = merge_hop(trans1['hop'], trans2['hop'])
    if hop is None:
        raise Exception('Merging transitions with conflict hop')
    
    return {
        'src': lambda ip: trans1['src'](ip) and trans2['src'](ip),
        'dst': lambda ip: trans1['dst'](ip) and trans2['dst'](ip),
        'pkt': merge_comp_list(trans1['pkt'], trans2['pkt']),
        'spg': merge_comp_list(trans1['spg'], trans2['spg']),
        'req': merged_req,
        'spu': trans1['spu'] + trans2['spu'],
        'hop': hop,
    }

def negate_comp(comp):
    if 'neg' in comp:
        return comp['neg']
    else:
        nc = {'neg': comp}
        if 'dep' in comp:
            nc['dep'] = comp['dep']
        def equ(c1, c2):
            if 'neg' in c1 and 'neg' in c2:
                return c1['neg']['equ'](c1['neg'], c2['neg']) and \
                    c2['neg']['equ'](c1['neg'], c2['neg'])
            else:
                return False
        nc['equ'] = equ
        return nc
