# sidfam/util.py -- Utility functions for building automaton more easily.
# Created by Correctizer on Jul 12, 2018.

from core import Resource


def current_hop_requirement(hop):
    if hop == '.':
        return lambda env: {}
    r = Resource(0)
    def req(env):
        if env['current_hop'] != hop:
            x = 1
        else:
            # print(env)
            x = 0
        return {r: x}
    return req
