# sidfam/prob.py -- Abstract interface for MILP solver.
# Created by Correctizer on Jul 9, 2018.

# IMPL: dict
#   <solver name>: () -> dict
#     crt: () -> <internal problem>
#     sol: (<internal problem>) -> <solution>
#     var: (<internal problem>) -> <var>
#     lim: (<internal problem>, <expr>, <factor>) -> None
# expr: dict
#   <var>: <factor>
# var: <_internal_>
# solution: dict
#   <var>: bool

IMPL = {}
try:
    from pulp import LpProblem, LpVariable, LpInteger
    IMPL['PuLP'] = lambda: {
        'crt': create_PuLP_problem,
        'sol': solve_PuLP_problem,
        'lim': constraint_PuLP_problem,
        'var': create_PuLP_variable,
    }
except ImportError:
    pass

# solve: (<A>, <b>) -> <x>
def convert_matrix_impl_to_variable(solve):
    def create_matrix_problem():
        return {
            'variables': 0,
            'constraint': [],
        }

    def create_matrix_variable(p):
        v = p['variables']
        p['variables'] += 1
        return v

    def constraint_matrix_problem(p, e, l):
        p['constraint'].append((e, l))

    def solve_matrix_problem(p):
        matrix_A, vector_b = [], []
        for e, l in p['constraint']:
            row = [0] * p['variables']
            for v, f in e.items():
                row[v] = f
            matrix_A.append(row)
            vector_b.append(l)
        vector_x = solve(matrix_A, vector_b)
        return {i: vector_x[i] for i in range(p['variables'])}
        
    return lambda: {
        'crt': create_matrix_problem,
        'var': create_matrix_variable,
        'lim': constraint_matrix_problem,
        'sol': solve_matrix_problem,
    }

# prob: dict
#   internal: <internal problem>
#   solver: <solver>

def create_problem(solver=None):
    if solver is None:
        solver = CHOSEN_SOLVER
    return {
        'internal': solver()['crt'](),
        'solver': solver,
    }

def problem_count(func):
    count = 0
    def create_problem():
        nonlocal count
        p = func(count)
        count += 1
        return p
    return create_problem

def create_variable(prob):
    return prob['solver']()['var'](prob['internal'])

def solve_problem(prob):
    return prob['solver']()['sol'](prob['internal'])

def constraint_problem_le(prob, expr, limit):
    return prob['solver']()['lim'](prob['internal'], expr, limit)

def constraint_problem_ge(prob, expr, limit):
    constraint_problem_le(prob, {v: -f for v, f in expr.items()}, -limit)

def constraint_problem_eq(prob, expr, limit):
    constraint_problem_le(prob, expr, limit)
    constraint_problem_ge(prob, expr, limit)


@problem_count
def create_PuLP_problem(n):
    p = LpProblem(f'problem #{n}')
    return {
        'internal': p,
        'variables': [],
    }

def create_PuLP_variable(prob):
    # print(f'Create variable {len(prob["variables"])}')
    v = LpVariable(f'var#{len(prob["variables"])}', 0, 1, cat=LpInteger)
    prob['variables'].append(v)
    return v

def constraint_PuLP_problem(prob, expr, limit):
    prob['internal'] += sum([v * f for v, f in expr.items()]) <= limit

def solve_PuLP_problem(prob):
    # print('Solve problem')
    from warnings import filterwarnings, catch_warnings
    with catch_warnings():
        filterwarnings('ignore', category=ResourceWarning)
        status = prob['internal'].solve()
    # print(f'status: {status}')
    if status != 1:
        raise Exception('The problem cannot be solved')
    return {
        var: var.value() > 0.5 for var in prob['variables']
    }


if len(IMPL) == 0:
    raise Exception('No avaiable solver found')

CHOSEN_SOLVER_NAME = 'PuLP'
CHOSEN_SOLVER = IMPL[CHOSEN_SOLVER_NAME]

def choose_custom_solver(solver):
    global CHOSEN_SOLVER, CHOSEN_SOLVER_NAME
    CHOSEN_SOLVER = solver
    CHOSEN_SOLVER_NAME = '<custom>'

def choose_solver_name(solver_name):
    global CHOSEN_SOLVER, CHOSEN_SOLVER_NAME
    if solver_name not in IMPL:
        raise Exception(f'Solver {solver_name} is not supported')
    CHOSEN_SOLVER_NAME = solver_name
    CHOSEN_SOLVER = IMPL[CHOSEN_SOLVER_NAME]

def chosen_solver_name():
    return CHOSEN_SOLVER_NAME
