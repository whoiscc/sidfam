from unittest import TestCase
from util import current_hop_requirement
from core import solve_auto


class TestCurrentHopRequirement(TestCase):
    def test_simple(self):
        auto = {
            'initial': 'q0', 'end': 'qe',
            'nodes': {
                'q0': [
                    {
                        'dest': 'q0',
                        'trans': {
                            'src': lambda ip: True, 'dst': lambda ip: ip == '<ip1>',
                            'pkt': [], 'spg': [], 'spu': [], 'hop': '.',
                            'req': lambda env: {},
                        },
                    }, {
                        'dest': 'qe',
                        'trans': {
                            'src': lambda ip: True, 'dst': lambda ip: ip == '<ip1>',
                            'pkt': [], 'spg': [], 'spu': [], 'hop': 'e',
                            'req': current_hop_requirement('B'),
                        },
                    },
                ],
            },
        }
        network = {
            'ip': {
                '<ip0>': 'A',
                '<ip1>': 'B',
                '<ip2>': 'C',
                '<ip3>': 'D',
            },
            'conn': {
                'A': ['B', 'D'],
                'B': ['A', 'C', 'D'],
                'C': ['B', 'D'],
                'D': ['A', 'B', 'C'],
            },
        }
        rule = solve_auto(auto, network)
        self.assertEqual(len(rule['B']), 3)

    def test_unsolved(self):
        auto = {
            'initial': 'q0', 'end': 'qe',
            'nodes': {
                'q0': [
                    {
                        'dest': 'q0',
                        'trans': {
                            'src': lambda ip: True, 'dst': lambda ip: True,
                            'pkt': [], 'spg': [], 'spu': [], 'hop': '.',
                            'req': lambda env: {},
                        },
                    }, {
                        'dest': 'qe',
                        'trans': {
                            'src': lambda ip: True, 'dst': lambda ip: True,
                            'pkt': [], 'spg': [], 'spu': [], 'hop': 'e',
                            'req': current_hop_requirement('B'),
                        },
                    },
                ],
            },
        }
        network = {
            'ip': {
                '<ip0>': 'A',
                '<ip1>': 'B',
                '<ip2>': 'C',
                '<ip3>': 'D',
            },
            'conn': {
                'A': ['B', 'D'],
                'B': ['A', 'C', 'D'],
                'C': ['B', 'D'],
                'D': ['A', 'B', 'C'],
            },
        }
        with self.assertRaises(Exception):
            solve_auto(auto, network)
