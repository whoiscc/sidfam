from unittest import TestCase
from prob import create_problem, chosen_solver_name, choose_solver_name


class TestCreateProblem(TestCase):
    def test_simple(self):
        p = create_problem()
        self.assertIsNotNone(p)

    def test_unknown_solver(self):
        old_solver_name = chosen_solver_name()
        with self.assertRaises(Exception):
            choose_solver('unknown')
            create_problem()
        choose_solver_name(old_solver_name)
