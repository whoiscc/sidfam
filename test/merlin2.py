from unittest import TestCase
from lang import merlin, merlin_regex
from core import Resource, solve_auto


class TestMerlin2(TestCase):
    def test_complicated(self):
        R = Resource
        bandwidth = {}
        for hop1 in ['A', 'B', 'C', 'D']:
            for hop2 in ['A', 'B', 'C', 'D']:
                bandwidth[hop1, hop2] = R(100)
        auto, bw_getter = merlin({
            'path': {
                'x': {
                    'src': lambda ip: ip == '<ip0>',
                    'dst': lambda ip: ip == '<ip2>',
                    'pkt': [], 'regex': merlin_regex('<.>*<B><D><.>*', {}),
                },
                'y': {
                    'src': lambda ip: ip == '<ip2>',
                    'dst': lambda ip: ip == '<ip0>',
                    'pkt': [], 'regex': merlin_regex('<.>*<D>[^A,C]<.>*', {}),
                },
            },
            'rule': [],
        })
        bw_getter(bandwidth)
        network = {
            'ip': {
                '<ip0>': 'A',
                '<ip1>': 'B',
                '<ip2>': 'C',
                '<ip3>': 'D',
            },
            'conn': {
                'A': ['B', 'D'],
                'B': ['A', 'C', 'D'],
                'C': ['B', 'D'],
                'D': ['A', 'B', 'C'],
            },
        }
        rule = solve_auto(auto, network)
        # from pprint import pprint
        # pprint(rule)
        for hop in ['A', 'B', 'C', 'D']:
            self.assertIn(hop, rule)
            self.assertEqual(len(rule[hop]), 2)

    def test_embedded(self):
        R = Resource
        bandwidth = {}
        for hop1 in ['A', 'B', 'C', 'D']:
            for hop2 in ['A', 'B', 'C', 'D']:
                bandwidth[hop1, hop2] = R(100)
        bandwidth['C', 'D'] = bandwidth['B', 'D'] = R(5)
        inner_auto = {
            'initial': 'q0', 'end': 'qe',
            'nodes': {
                'q0': [
                    {
                        'dest': 'qe', 'trans': {
                            'src': lambda ip: True, 'dst': lambda ip: True,
                            'pkt': [], 'spg': [], 'req': lambda env: {}, 'hop': 'e',
                            'spu': [{'dep': {'x'}}],
                        },
                    },
                ],
            },
        }
        auto, bw_getter = merlin({
            'path': {
                'x': {
                    'src': lambda ip: ip == '<ip0>',
                    'dst': lambda ip: ip == '<ip2>',
                    'pkt': [], 'regex': merlin_regex('<.><?x><.>', {'x': inner_auto}),
                },
                'y': {
                    'src': lambda ip: ip == '<ip2>',
                    'dst': lambda ip: ip == '<ip0>',
                    'pkt': [], 'regex': merlin_regex('<.><?x><.>', {'x': inner_auto}),
                },
            },
            'rule': [
                {'type': 'min', 'path': ['y'], 'limit': 10},
            ],
        })
        bw_getter(bandwidth)
        network = {
            'ip': {
                '<ip0>': 'A',
                # '<ip1>': 'B',
                '<ip2>': 'C',
                # '<ip3>': 'D',
            },
            'conn': {
                'A': ['B', 'D'],
                'B': ['A', 'C', 'D'],
                'C': ['B', 'D'],
                'D': ['A', 'B', 'C'],
            },
        }
        rule = solve_auto(auto, network)
        for hop in rule:
            if hop != 'B':
                for ma in rule[hop]:
                    self.assertEqual(ma['action']['spu'], [])

    def test_or(self):
        R = Resource
        bandwidth = {}
        for hop1 in ['A', 'B', 'C', 'D']:
            for hop2 in ['A', 'B', 'C', 'D']:
                bandwidth[hop1, hop2] = R(100)
        auto, bw_getter = merlin({
            'path': {
                'x': {
                    'src': lambda ip: ip != '<ip3>',
                    'dst': lambda ip: ip == '<ip0>',
                    'pkt': [], 'regex': merlin_regex('(<.>*<B><D><.>*)|(<.>*<B><C><.>*)', {}),
                },
            },
            'rule': [],
        })
        bw_getter(bandwidth)
        network = {
            'ip': {
                '<ip0>': 'A',
                '<ip1>': 'B',
                '<ip2>': 'C',
                '<ip3>': 'D',
            },
            'conn': {
                'A': ['B', 'D'],
                'B': ['A', 'C', 'D'],
                'C': ['B', 'D'],
                'D': ['A', 'B', 'C'],
            },
        }
        # from pprint import pprint
        # pprint(auto)
        rule = solve_auto(auto, network)
        for ma in rule['B']:
            self.assertNotEqual(ma['action']['next_hop'], 'A')
