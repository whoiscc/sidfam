# Created by Correctizer on Jul 14, 2018.
from unittest import TestCase, skipUnless
from sys import argv
from core import solve_auto, Resource, same_comp_list, negate_comp
from lang import merlin, merlin_regex, snap
from oper import intersection

class TestMerlinSNAP(TestCase):
    def test_simple(self):
        auto1, bw_setter = merlin({
            'path': {
                'x': {
                    'src': lambda ip: True,
                    'dst': lambda ip: True,
                    'regex': merlin_regex('<.>*<B><.>*')
                },
            },
            'rule': [
                {'type': 'min', 'path': ['x'], 'limit': 10},
            ],
        })
        pkt = {'equ': lambda x, y: x is y}
        spu = {'equ': lambda x, y: x is y, 'dep': {'p'}}
        auto2 = snap({
            'type': 'if', 'value': {'guard': {'pkt': [pkt]},
                'true': {
                    'spu': [spu], 'expr': {'type': 'id'},
                },
                'false': {'expr': {'type': 'id'}},
            },
        })
        # from pprint import pprint
        # pprint(merlin_regex('<.>*<B><.>*'))
        # pprint(auto1)

        auto3 = intersection(auto1, auto2)
        network = {
            'ip': {'<ip0>': 'A', '<ip1>': 'B', '<ip2>': 'C', '<ip3>': 'D'},
            # 'ip': {'<ip0>': 'A', '<ip1>': 'B'},
            'conn': {
                'A': ['B', 'D'], 'B': ['A', 'C', 'D'],
                'C': ['B', 'D'], 'D': ['A', 'B', 'C'],
            },
        }
        bw = {}
        bw['A', 'B'] = bw['B', 'A'] = bw['B', 'C'] = bw['C', 'B'] = 30
        bw['B', 'D'] = bw['D', 'B'] = 30
        bw['A', 'D'] = bw['C', 'D'] = bw['D', 'A'] = bw['D', 'C'] = 5
        bw_res = {}
        for edge in bw:
            bw_res[edge] = Resource(bw[edge], True)
        bw_setter(bw_res)

        rule = solve_auto(auto3, network)
        # from pprint import pprint
        # pprint(rule)
        for hop in rule:
            if hop != 'B':
                for ma in rule[hop]:
                    self.assertNotIn(spu, ma['action']['spu'])
                    self.assertIn(ma['action']['next_hop'], ['B', 'e'])

        bw_res['A', 'B'] = Resource(29, True)
        bw_setter(bw_res)

        with self.assertRaises(Exception):
            solve_auto(auto, network)

    def test_drop(self):
        auto1, bw_setter = merlin({
            'path': {
                'x': {
                    'src': lambda ip: True,
                    'dst': lambda ip: True,
                    'regex': merlin_regex('<.>*<B><.>*')
                },
            },
            'rule': [
                {'type': 'min', 'path': ['x'], 'limit': 10},
            ],
        })
        spg = {'equ': lambda x, y: x is y, 'dep': set()}
        spu = {'equ': lambda x, y: x is y, 'dep': {'p'}}
        auto2 = snap({
            'type': 'if', 'value': {'guard': {'spg': [spg]},
                'true': {
                    'spu': [spu], 'expr': {'type': 'id'},
                },
                'false': {
                    'spu': [], 'expr': {'type': 'drop'}},
            },
        })
        auto3 = intersection(auto1, auto2)
        network = {
            'ip': {'<ip0>': 'A', '<ip1>': 'B', '<ip2>': 'C', '<ip3>': 'D'},
            # 'ip': {'<ip0>': 'A', '<ip1>': 'B'},
            'conn': {
                'A': ['B', 'D'], 'B': ['A', 'C', 'D'],
                'C': ['B', 'D'], 'D': ['A', 'B', 'C'],
            },
        }
        bw = {}
        bw['A', 'B'] = bw['B', 'A'] = bw['B', 'C'] = bw['C', 'B'] = 30
        bw['B', 'D'] = bw['D', 'B'] = 30
        bw['A', 'D'] = bw['C', 'D'] = bw['D', 'A'] = bw['D', 'C'] = 5
        bw_res = {}
        for edge in bw:
            bw_res[edge] = Resource(bw[edge], True)
        bw_setter(bw_res)

        with self.assertRaises(Exception):
            solve_auto(auto, network)

        auto1['nodes'][auto1['initial']].append({
            'dest': auto1['end'], 'trans': {
                'src': lambda ip: True, 'dst': lambda ip: True,
                'pkt': [], 'spg': [], 'spu': [], 'req': lambda env: {},
                'hop': 'd',
            }
        })
        auto3 = intersection(auto1, auto2)

        rule = solve_auto(auto3, network)
        # from pprint import pprint
        # pprint(rule)
        for hop in rule:
            if hop != 'B':
                for ma in rule[hop]:
                    self.assertNotIn(spu, ma['action']['spu'])
                    self.assertIn(ma['action']['next_hop'], ['B', 'e', 'd'])
            else:
                for ma in rule[hop]:
                    if ma['match']['src'] != '<ip1>':
                        self.assertNotEqual(ma['action']['next_hop'], 'd')

        bw_res['A', 'B'] = Resource(29, True)
        bw_setter(bw_res)

        with self.assertRaises(Exception):
            solve_auto(auto, network)

    @skipUnless('--long-term' in argv, 'long term test cases does not execute')
    def test_different_way(self):
        auto1, bw_setter = merlin({
            'path': {
                'x': {
                    'src': lambda ip: True,
                    'dst': lambda ip: True,
                    'regex': merlin_regex('<.>*<B><.>*')
                },
            },
            'rule': [
                {'type': 'min', 'path': ['x'], 'limit': 10},
            ],
        })
        auto1['nodes'][auto1['initial']].append({
            'dest': auto1['initial'], 'trans': {
                'src': lambda ip: True, 'dst': lambda ip: True,
                'pkt': [], 'spg': [], 'spu': [], 'req': lambda env: {}, 
                'hop': '.',
            }
        })
        auto1['nodes'][auto1['initial']].append({
            'dest': auto1['end'], 'trans': {
                'src': lambda ip: True, 'dst': lambda ip: True,
                'pkt': [], 'spg': [], 'spu': [], 'req': lambda env: {}, 
                'hop': 'd',
            }
        })
        pkt = {'equ': lambda x, y: x is y}
        spx = {'equ': lambda x, y: x is y, 'dep': {'x'}}
        spy = {'equ': lambda x, y: x is y, 'dep': {'y'}}
        spu = {'equ': lambda x, y: x is y, 'dep': {'u'}}
        auto2 = snap({
            'type': 'if', 'value': {'guard': {
                    'src': lambda ip: ip == '<ip0>',
                    'dst': lambda ip: ip == '<ip2>',
                },
                'true': {'expr': {
                    'type': 'if', 'value': {'guard': {'spg': [spx]},
                        'true': {
                            'spu': [spu], 
                            'expr': {
                                'type': 'if', 'value': {'guard': {'spg': [spy]},
                                    'true': {'expr': {'type': 'id'}},
                                    'false': {'expr': {'type': 'drop'}},
                                }
                            },
                        },
                        'false': {'expr': {'type': 'id'}},
                    },
                }},
                'false': {'expr': {'type': 'drop'}},
            }
        })
        # from pprint import pprint
        # pprint(auto2)

        auto3 = intersection(auto1, auto2)
        # from pprint import pprint
        # pprint(auto3)
        network = {
            # 'ip': {'<ip0>': 'A', '<ip1>': 'B', '<ip2>': 'C', '<ip3>': 'D'},
            'ip': {'<ip0>': 'A', '<ip2>': 'C'},
            'conn': {
                'A': ['B', 'D'], 'B': ['A', 'C', 'D'],
                'C': ['B', 'D'], 'D': ['A', 'B', 'C'],
            },
        }
        bw = {}
        bw['A', 'B'] = bw['B', 'A'] = bw['B', 'C'] = bw['C', 'B'] = 100
        bw['B', 'D'] = bw['D', 'B'] = 100
        bw['A', 'D'] = bw['C', 'D'] = bw['D', 'A'] = bw['D', 'C'] = 100
        bw_res = {}
        for edge in bw:
            bw_res[edge] = Resource(bw[edge], True)
        bw_setter(bw_res)

        rule = solve_auto(auto3, network)
        from pprint import pprint
        pprint(rule)

        for hop in rule:
            for ma in rule[hop]:
                self.assertFalse(same_comp_list(ma['match']['spg'], [spx, spy]))
                self.assertFalse(same_comp_list(ma['match']['spg'], [negate_comp(spx), negate_comp(spy)]))
                self.assertFalse(same_comp_list(ma['match']['spg'], [negate_comp(spx), spy]))
                self.assertFalse(same_comp_list(ma['match']['spg'], [spx, negate_comp(spy)]))
                if hop == 'A':
                    self.assertFalse(same_comp_list(ma['match']['spg'], [spx]))
                    self.assertFalse(same_comp_list(ma['match']['spg'], [negate_comp(spx)]))
                    self.assertFalse(same_comp_list(ma['match']['spg'], [spy]))
                    self.assertFalse(same_comp_list(ma['match']['spg'], [negate_comp(spy)]))
                    if ma['match']['src'] == '<ip0>':
                        self.assertEqual(ma['action']['next_hop'], 'B')
                elif hop == 'D':
                    self.assertFalse(same_comp_list(ma['match']['spg'], [spx]))
                    self.assertFalse(same_comp_list(ma['match']['spg'], [negate_comp(spx)]))
                    if ma['match']['src'] == '<ip0>':
                        self.assertEqual(ma['action']['next_hop'], 'C')
                elif hop == 'B':
                    self.assertFalse(same_comp_list(ma['match']['spg'], [spy]))
                    self.assertFalse(same_comp_list(ma['match']['spg'], [negate_comp(spy)]))
                    if same_comp_list(ma['match']['spg'], [spx]):
                        self.assertEqual(ma['action']['next_hop'], 'D')
                    elif same_comp_list(ma['match']['spg'], [negate_comp(spx)]):
                        self.assertEqual(ma['action']['next_hop'], 'B') 
                else:
                    self.assertFalse(same_comp_list(ma['match']['spg'], [spx]))
                    self.assertFalse(same_comp_list(ma['match']['spg'], [negate_comp(spx)]))
                    self.assertFalse(same_comp_list(ma['match']['spg'], [spy]))
                    self.assertFalse(same_comp_list(ma['match']['spg'], [negate_comp(spy)]))
