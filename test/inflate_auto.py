from unittest import TestCase
from core import inflate_auto


class TestInflateAuto(TestCase):
    def test_simple(self):
        pkt = 'pkt placeholder'
        spg = 'spg placeholder'
        req = 'req placeholder'
        spu = 'spu placeholder'

        def trans(src, dst, hop):
            return {
                'src': src, 'dst': dst, 'pkt': pkt, 'spg': spg,
                'req': req, 'spu': spu, 'hop': hop,
            }

        def ip(n):
            return f'ip{n} placeholder'

        auto = {
            'initial': 'q0', 'end': 'qe',
            'nodes': {
                'q0': [
                    {
                        'dest': 'q1',
                        'trans': trans(
                            lambda x: x == ip(0),
                            lambda x: x >= ip(1),
                            'A',
                        ),
                    },
                ],
                'q1': [
                    {
                        'dest': 'q1',
                        'trans': trans(
                            lambda x: x <= ip(1),
                            lambda x: True,
                            '.',
                        ),
                    }, {
                        'dest': 'qe',
                        'trans': trans(
                            lambda x: x == ip(2),
                            lambda x: x == ip(0),
                            'e',
                        )
                    }
                ]
            },
        }

        ia = inflate_auto(auto, [ip(0), ip(1), ip(2)], ['A', 'B'])
        src_dst_list = []
        for edge in ia['nodes']['q0']:
            self.assertEqual(edge['dest'], 'q1')
            self.assertEqual(edge['sdt']['hop'], 'A')
            sdt = edge['sdt']
            src_dst_list.append((sdt['src'], sdt['dst']))
        self.assertEqual(
            sorted(src_dst_list),
            sorted([(ip(0), ip(1)), (ip(0), ip(2))])
        )
        src_dst_hop_list = []
        unique_edge_appeared = False
        for edge in ia['nodes']['q1']:
            if edge['dest'] == 'qe':
                self.assertFalse(unique_edge_appeared)
                unique_edge_appeared = True
                self.assertEqual(edge['sdt']['hop'], 'e')
                self.assertEqual(edge['sdt']['src'], ip(2))
                self.assertEqual(edge['sdt']['dst'], ip(0))
            else:
                src_dst_hop_list.append(
                    (
                        edge['sdt']['src'],
                        edge['sdt']['dst'],
                        edge['sdt']['hop'],
                    ),
                )
        self.assertEqual(
            sorted(src_dst_hop_list),
            sorted([
                (ip(0), ip(1), 'A'), (ip(0), ip(1), 'B'),
                (ip(0), ip(2), 'A'), (ip(0), ip(2), 'B'),
                (ip(1), ip(0), 'A'), (ip(1), ip(0), 'B'),
                (ip(1), ip(2), 'A'), (ip(1), ip(2), 'B'),
            ])
        )
