from unittest import TestCase
from prob import convert_matrix_impl_to_variable, create_problem, \
    create_variable, constraint_problem_le, solve_problem, chosen_solver_name, \
    choose_custom_solver, choose_solver_name


class TestConvertMatrixImplToVariable(TestCase):
    def test_simple(self):
        def mock_solve(A, b):
            self.assertListEqual(
                A,
                [
                    [1, 1, 1],
                    [2, 3, 4],
                ],
            )
            self.assertListEqual(
                b, [5, 6],
            )
            return [7, 8, 9]

        solver = convert_matrix_impl_to_variable(mock_solve)
        p = create_problem(solver=solver)
        v = [create_variable(p) for _ in range(3)]
        constraint_problem_le(p, {v[0]: 1, v[1]: 1, v[2]: 1}, 5)
        constraint_problem_le(p, {v[0]: 2, v[1]: 3, v[2]: 4}, 6)
        s = solve_problem(p)
        self.assertEqual(s, {v[0]: 7, v[1]: 8, v[2]: 9})

    def test_global(self):
        def mock_solve(A, b):
            self.assertListEqual(
                A,
                [
                    [1, 1, 1],
                    [2, 3, 4],
                ],
            )
            self.assertListEqual(
                b, [5, 6],
            )
            return [7, 8, 9]

        solver = convert_matrix_impl_to_variable(mock_solve)
        old_solver_name = chosen_solver_name()
        choose_custom_solver(solver)
        self.assertEqual(chosen_solver_name(), '<custom>')
        p = create_problem()
        v = [create_variable(p) for _ in range(3)]
        constraint_problem_le(p, {v[0]: 1, v[1]: 1, v[2]: 1}, 5)
        constraint_problem_le(p, {v[0]: 2, v[1]: 3, v[2]: 4}, 6)
        s = solve_problem(p)
        self.assertEqual(s, {v[0]: 7, v[1]: 8, v[2]: 9})
        choose_solver_name(old_solver_name)
