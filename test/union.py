from unittest import TestCase
from oper import union
from core import solve_auto, Resource


class TestUnion(TestCase):
    def test_simple(self):
        network = {
            'ip': {
                '<ip0>': 'A',
                '<ip1>': 'B',
                '<ip2>': 'C',
                '<ip3>': 'D',
            },
            'conn': {
                'A': ['B', 'D'],
                'B': ['A', 'C', 'D'],
                'C': ['B', 'D'],
                'D': ['A', 'B', 'C'],
            },
        }

        def edge(dest, hop, req):
            return {
                'dest': dest,
                'trans': {
                    'src': lambda ip: True,
                    'dst': lambda ip: True,
                    'pkt': [], 'spg': [], 'req': req, 'spu': [],
                    'hop': hop
                }
            }

        R = Resource
        bandwidth = {
            'A': {'B': R(100), 'D': R(100)},
            'B': {'A': R(100), 'C': R(100), 'D': R(100)},
            'C': {'B': R(100), 'D': R(100)},
            'D': {'A': R(100), 'B': R(100), 'C': R(100)},
        }

        no_req = lambda env: {}
        def bw_req(x):
            return lambda env: {
                bandwidth[env['current_hop']][env['next_hop']]: x
            }

        auto1 = {
            'initial': 'q0', 'end': 'qe',
            'nodes': {
                'q0': [
                    edge('q0', '.', bw_req(10)),
                    edge('q1', 'B', bw_req(10)),
                ],
                'q1': [
                    edge('q1', '.', bw_req(10)),
                    edge('qe', 'e', no_req),
                ],
            },
        }

        auto2 = {
            'initial': 'q0', 'end': 'qe',
            'nodes': {
                'q0': [
                    edge('q0', '.', bw_req(10)),
                    edge('q1', 'D', bw_req(10)),
                ],
                'q1': [
                    edge('q1', '.', bw_req(10)),
                    edge('qe', 'e', no_req),
                ],
            },
        }

        auto3 = union(auto1, auto2)
        rule = solve_auto(auto3, network)
        for src in ['<ip0>', '<ip1>', '<ip2>', '<ip3>']:
            for dst in ['<ip0>', '<ip1>', '<ip2>', '<ip3>']:
                if src == dst:
                    continue
                appeared = False
                for ma in rule['B'] + rule['D']:
                    if ma['match']['src'] == src and ma['match']['dst'] == dst:
                        appeared = True
                self.assertTrue(appeared)
