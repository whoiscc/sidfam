from unittest import TestCase
from lang import snap
from core import solve_auto, negate_comp, same_comp_list


class TestSNAP(TestCase):
    def test_simple(self):
        spg = {'dep': {'x'}, 'equ': lambda c1, c2: c1 is c2}
        spu = {'dep': {'x'}}
        snap_source = {
            'type': 'if',
            'value': {
                'guard': {
                    'src': lambda ip: ip == '<ip0>',
                    'dst': lambda ip: ip == '<ip2>',
                    'pkt': [], 'spg': [spg],
                },
                'true': {
                    'spu': [spu], 'expr': {'type': 'id', 'value': None},
                },
                'false': {
                    'spu': [], 'expr': {'type': 'drop', 'value': None},
                },
            }
        }
        # snap_source = {
        #     'type': 'drop',
        #     'value': None,
        # }
        auto = snap(snap_source)
        # from pprint import pprint
        # pprint(auto)
        network = {
            'ip': {
                '<ip0>': 'A',
                '<ip1>': 'B',
                '<ip2>': 'C',
                '<ip3>': 'D',
            },
            'conn': {
                'A': ['B', 'D'],
                'B': ['A', 'C', 'D'],
                'C': ['B', 'D'],
                'D': ['A', 'B', 'C'],
            },
        }
        rule = solve_auto(auto, network)
        # from pprint import pprint
        # pprint(rule)
        for hop in rule:
            for ma in rule[hop]:
                if ma['match']['spg'] == [spg]:
                    self.assertEqual(ma['action']['spu'], [spu])

    def test_direct(self):
        spg1 = {'dep': {'x'}, 'equ': lambda c1, c2: c1 is c2}
        spg2 = {'dep': {'y'}, 'equ': lambda c1, c2: c1 is c2}
        spu = {'dep': {'z'}}
        snap_source = {
            'type': 'if',
            'value': {
                'guard': {
                    'src': lambda ip: ip == '<ip0>',
                    'dst': lambda ip: ip == '<ip1>',
                    'pkt': [], 'spg': [spg1],
                },
                'true': {
                    'spu': [spu], 'expr': {
                        'type': 'if', 'value': {
                            'guard': {'spg': [spg2]},
                            'true': {'expr': {'type': 'id', 'value': None}},
                            'false': {'expr': {'type': 'drop', 'value': None}},
                        }
                    },
                },
                'false': {
                    'spu': [], 'expr': {'type': 'drop', 'value': None},
                },
            }
        }
        auto = snap(snap_source)
        network = {
            'ip': {
                '<ip0>': 'A',
                '<ip1>': 'B',
                '<ip2>': 'C',
                '<ip3>': 'D',
            },
            'conn': {
                'A': ['B'],
                'B': ['A', 'C', 'D'],
                'C': ['B', 'D'],
                'D': ['A', 'B', 'C'],
            },
        }
        rule = solve_auto(auto, network)
        # from pprint import pprint
        # pprint(rule)
        for ma in rule['A']:
            self.assertNotEqual(ma['match'], [spg2])

    def test_inherit(self):
        spg1 = {'dep': {'x'}, 'equ': lambda c1, c2: c1 is c2}
        spg2 = {'dep': {'x'}, 'equ': lambda c1, c2: c1 is c2}
        spu1 = {'dep': {'y'}}
        spu2 = {'dep': {'z'}}
        snap_source = {
            'type': 'if',
            'value': {
                'guard': {
                    'src': lambda ip: True,
                    'dst': lambda ip: True,
                    'pkt': [], 'spg': [spg1],
                },
                'true': {
                    'spu': [], 'expr': {
                        'type': 'if', 'value': {'guard': {'spg': [spg2]},
                            'true': {
                                'expr': {'type': 'id', 'value': None}
                            },
                            'false': {
                                'spu': [spu2],
                                'expr': {'type': 'drop', 'value': None},
                            },
                        }
                    },
                },
                'false': {
                    'spu': [], 'expr': {
                        'type': 'if', 'value': {'guard': {'spg': [spg2]},
                            'true': {
                                'spu': [spu1],
                                'expr': {'type': 'drop', 'value': None},
                            },
                            'false': {
                                'expr': {'type': 'id', 'value': None},
                            }
                        }
                    },
                },
            }
        }
        auto = snap(snap_source)
        network = {
            'ip': {
                '<ip0>': 'A',
                '<ip1>': 'B',
                '<ip2>': 'C',
                '<ip3>': 'D',
            },
            'conn': {
                'A': ['B', 'D'],
                'B': ['A', 'C', 'D'],
                'C': ['B', 'D'],
                'D': ['A', 'B', 'C'],
            },
        }
        rule = solve_auto(auto, network)
        neg_guard_count1, neg_guard_count2 = 0, 0
        for hop in rule:
            for ma in rule[hop]:
                if same_comp_list(ma['match']['spg'], [negate_comp(spg1), spg2]):
                    neg_guard_count1 += 1
                    self.assertEqual(ma['action']['spu'], [spu1])
                elif same_comp_list(
                        ma['match']['spg'], [spg1, negate_comp(spg2)]):
                    neg_guard_count2 += 1
                    self.assertEqual(ma['action']['spu'], [spu2])
        self.assertEqual(neg_guard_count1, 12)
        self.assertEqual(neg_guard_count2, 12)
