from unittest import TestCase
from core import closure


class TestClousure(TestCase):
    def test_simple(self):
        trans = 'trans placeholder'
        auto = {
            'initial': 'q0',
            'end': 'qe',
            'nodes': {
                'q0': [
                    {'trans': trans, 'dest': 'q1'},
                    {'trans': 'epsilon', 'dest': 'q2'},
                ],
                'q1': [
                    {'trans': 'epsilon', 'dest': 'q0'},
                ],
                'q2': [
                    {'trans': 'epsilon', 'dest': 'q3'},
                    {'trans': trans, 'dest': 'q1'},
                ],
                'q3': [
                    {'trans': 'epsilon', 'dest': 'q2'},
                    {'trans': trans, 'dest': 'qe'},
                ],
            },
        }
        self.assertEqual(
            closure(auto, {'q0'}),
            {'q0', 'q2', 'q3'},
        )
        self.assertEqual(
            closure(auto, {'q1'}),
            {'q0', 'q1', 'q2', 'q3'}
        )
        self.assertEqual(
            closure(auto, {'q2'}),
            {'q2', 'q3'}
        )
        self.assertEqual(
            closure(auto, {'q3'}),
            {'q2', 'q3'}
        )
        self.assertEqual(
            closure(auto, {'q1', 'q2'}),
            {'q0', 'q1', 'q2', 'q3'}
        )
        self.assertEqual(
            closure(auto, {'q0', 'q2'}),
            {'q0', 'q2', 'q3'}
        )
