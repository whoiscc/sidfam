from unittest import TestCase
from core import goto_generator


class TestGotoGenerator(TestCase):
    def test_simple(self):
        def trans(n):
            return f'trans{n} placeholder'
        auto = {
            'initial': 'q0',
            'end': 'qe',
            'nodes': {
                'q0': [
                    {'trans': trans(0), 'dest': 'q1'},
                    {'trans': 'epsilon', 'dest': 'q2'},
                ],
                'q1': [
                    {'trans': 'epsilon', 'dest': 'q0'},
                ],
                'q2': [
                    {'trans': 'epsilon', 'dest': 'q3'},
                    {'trans': trans(1), 'dest': 'q1'},
                    {'trans': trans(2), 'dest': 'q2'}
                ],
                'q3': [
                    {'trans': 'epsilon', 'dest': 'q2'},
                    {'trans': trans(3), 'dest': 'qe'},
                ],
            },
        }
        self.assertEqual(
            {trans: dest for trans, dest in goto_generator(auto, {'q0'})},
            {trans(0): {'q0', 'q1', 'q2', 'q3'}}
        )
        self.assertEqual(
            {trans: dest for trans, dest in goto_generator(auto, {'q1'})},
            {}
        )
        self.assertEqual(
            {trans: dest for trans, dest in goto_generator(auto, {'q2'})},
            {trans(1): {'q0', 'q1', 'q2', 'q3'}, trans(2): {'q2', 'q3'}}
        )
        self.assertEqual(
            {trans: dest for trans, dest in goto_generator(auto, {'q3'})},
            {trans(3): {'qe'}}
        )
        self.assertEqual(
            {trans: dest for trans, dest in goto_generator(auto, {'q2', 'q3'})},
            {
                trans(1): {'q0', 'q1', 'q2', 'q3'},
                trans(2): {'q2', 'q3'},
                trans(3): {'qe'},
            }
        )
