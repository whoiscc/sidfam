from unittest import TestCase
from oper import symmetric_difference
from core import solve_auto, Resource


class TestSymmetricDifference(TestCase):
    def test_simple(self):
        network = {
            'ip': {
                '<ip0>': 'A',
                '<ip1>': 'B',
                '<ip2>': 'C',
                '<ip3>': 'D',
            },
            'conn': {
                'A': ['B', 'D'],
                'B': ['A', 'C', 'D'],
                'C': ['B', 'D'],
                'D': ['A', 'B', 'C'],
            },
        }

        def edge(dest, hop, req):
            return {
                'dest': dest,
                'trans': {
                    'src': lambda ip: ip != '<ip1>',
                    'dst': lambda ip: ip != '<ip1>',
                    'pkt': [], 'spg': [], 'req': req, 'spu': [],
                    'hop': hop
                }
            }

        R = Resource
        bandwidth = {
            'A': {'B': R(100), 'D': R(5)},
            'B': {'A': R(100), 'C': R(100), 'D': R(100)},
            'C': {'B': R(100), 'D': R(100)},
            'D': {'A': R(100), 'B': R(100), 'C': R(100)},
        }

        no_req = lambda env: {}
        def bw_req(x):
            return lambda env: {
                bandwidth[env['current_hop']][env['next_hop']]: x
            }

        auto1 = {
            'initial': 'q0', 'end': 'qe',
            'nodes': {
                'q0': [
                    edge('q0', '.', bw_req(10)),
                    edge('q1', 'B', bw_req(10)),
                ],
                'q1': [
                    edge('q2', 'C', bw_req(10)),
                ],
                'q2': [
                    edge('q2', '.', bw_req(10)),
                    edge('qe', 'e', no_req),
                ]
            },
        }

        auto2 = {
            'initial': 'q0', 'end': 'qe',
            'nodes': {
                'q0': [
                    edge('q0', 'A', bw_req(10)),
                    edge('q0', 'C', bw_req(10)),
                    edge('q0', 'D', bw_req(10)),
                    edge('qe', 'e', no_req),
                ],
            },
        }

        auto3 = symmetric_difference(auto1, auto2)
        rule = solve_auto(auto3, network)
        for ma in rule['B']:
            self.assertEqual(ma['action']['next_hop'], 'C')
        