from unittest import TestCase
from core import build_problem_path
from prob import create_problem, solve_problem


class TestBuildProblemPath(TestCase):
    def test_simple(self):
        def vert(hop):
            return {'other': 'other placeholder', 'next_hop': hop}

        pg = {
            'vert': [
                vert('A'), vert('D'), vert('B'), vert('C'), vert('A'), vert('e'),
            ],
            'edge': [
                (0, 1), (0, 2), (1, 2), (2, 3), (3, 1), (3, 2), (2, 4), (2, 5),
            ]
        }
        pb = create_problem()
        pgpv = build_problem_path(pb, pg)
        result_pg, result_pb, edge_var = pgpv['pg'], pgpv['pb'], pgpv['ev']
        self.assertIs(result_pg, pg)
        self.assertIs(result_pb, pb)
        # print(pb)
        sol = solve_problem(pb)
        self.assertTrue(sol[edge_var[0, 1]] != sol[edge_var[0, 2]])
        self.assertTrue(sol[edge_var[2, 5]])
        for vert_index in [1, 2, 3, 4]:
            enter, leave = [], []
            for edge in pg['edge']:
                if edge[0] == vert_index:
                    enter.append(edge)
                if edge[1] == vert_index:
                    leave.append(edge)
            total_enter = sum([sol[edge_var[edge]] for edge in enter])
            total_leave = sum([sol[edge_var[edge]] for edge in leave])
            self.assertTrue(total_enter <= 1)
            self.assertTrue(total_leave <= 1)
            self.assertTrue(total_enter == total_leave)
