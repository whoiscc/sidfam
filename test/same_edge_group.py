from unittest import TestCase
from core import same_edge_group


class TestSameEdgeGroup(TestCase):
    def test_simple(self):
        def comp(n):
            return {
                'str': 'comp str placeholder',
                'dep': [],
                'equ': lambda comp1, comp2: comp1['index'] == comp2['index'],
                'index': n
            }

        def ip(n):
            return f'ip{n} placeholder'

        req = 'req placeholder'
        spu = 'spu placeholder'

        edge1 = {
            'dest': 'q0',
            'sdt': {
                'hop': 'A',
                'src': ip(0), 'dst': ip(1),
                'pkt': [comp(0), comp(1), comp(2)],
                'spg': [comp(3), comp(4), comp(5)],
                'req': req, 'spu': spu,
            },
        }
        edge2 = {
            'dest': 'q1',
            'sdt': {
                'hop': 'B',
                'src': ip(0), 'dst': ip(1),
                'pkt': [comp(2), comp(0), comp(1)],
                'spg': [comp(5), comp(4), comp(3)],
                'req': req, 'spu': spu,
            },
        }
        edge3 = {
            'dest': 'q0',
            'sdt': {
                'hop': 'A',
                'src': ip(1), 'dst': ip(1),
                'pkt': [comp(2), comp(0), comp(1)],
                'spg': [comp(5), comp(4), comp(3)],
                'req': req, 'spu': spu,
            },
        }
        edge4 = {
            'dest': 'q0',
            'sdt': {
                'hop': 'A',
                'src': ip(0), 'dst': ip(1),
                'pkt': [comp(2), comp(0)],
                'spg': [comp(5), comp(4), comp(3)],
                'req': req, 'spu': spu,
            },
        }
        edge5 = {
            'dest': 'q0',
            'sdt': {
                'hop': 'A',
                'src': ip(0), 'dst': ip(1),
                'pkt': [comp(2), comp(0), comp(1)],
                'spg': [comp(6), comp(4), comp(3)],
                'req': req, 'spu': spu,
            },
        }
        edge6 = {
            'dest': 'q0',
            'sdt': {
                'hop': 'A',
                'src': ip(0), 'dst': ip(1),
                'pkt': [comp(0), comp(1), comp(2)],
                'spg': [comp(3), comp(4), comp(5), comp(6)],
                'req': req, 'spu': spu,
            },
        }
        self.assertTrue(same_edge_group(edge1, edge2))
        self.assertFalse(same_edge_group(edge1, edge3))
        self.assertFalse(same_edge_group(edge1, edge4))
        self.assertFalse(same_edge_group(edge1, edge5))
        self.assertFalse(same_edge_group(edge1, edge6))
