from unittest import TestCase
from core import build_problem_deps, convert_fia_to_path_graph, build_problem_path
from prob import create_problem, solve_problem


class TestBuildProblemDeps(TestCase):
    def test_simple(self):
        # from now on, I have to use previous tested functions
        # to be more complex test cases
        def edges(dest, spu, hop_list, src, dst):
            return [
                {
                    'dest': dest,
                    'sdt': {
                        'src': src, 'dst': dst, 'hop': hop,
                        'pkt': [], 'spg': [], 'req': '<req>', 'spu': spu,
                    }
                }
                for hop in hop_list
            ]

        spu = {'dep': {'x'}}
        hops = ['A', 'B', 'C', 'D']

        def ia(src, dst):
            return {
                'initial': 'q0', 'end': 'qe',
                'nodes': {
                    'q0': edges('q0', [], hops, src, dst) + \
                        edges('q1', [spu], hops, src, dst) + \
                        edges('q2', [spu], ['e'], src, dst),
                    'q1': edges('q1', [], hops, src, dst) + \
                        edges('q2', [], ['e'], src, dst)
                },
            }
        fia_list = [
            {'src': '<ip0>', 'dst': '<ip1>', 'ia': ia('<ip0>', '<ip2>')},
            {'src': '<ip0>', 'dst': '<ip3>', 'ia': ia('<ip1>', '<ip3>')},
        ]
        nw = {
            'ip': {
                '<ip0>': 'A',
                '<ip1>': 'B',
                '<ip2>': 'C',
                '<ip3>': 'D',
            },
            'conn': {
                'A': ['B', 'D'],
                'B': ['A', 'C', 'D'],
                'C': ['B', 'D'],
                'D': ['A', 'B', 'C'],
            },
        }

        pb = create_problem()
        pgpv = []
        for fia in fia_list:
            pg = convert_fia_to_path_graph(fia, nw)
            pgpv.append(build_problem_path(pb, pg))
        build_problem_deps(pgpv)
        # print(pb)
        sl = solve_problem(pb)
        # print(len(sl))
        selected_hop = None
        for pg, ev in [(x['pg'], x['ev']) for x in pgpv]:
            for edge, var in ev.items():
                # print(var, sl[var])
                if sl[var] and pg['vert'][edge[1]]['spu'] != []:
                    # print(pg['vert'][edge[0]], pg['vert'][edge[1]])
                    hop = pg['vert'][edge[0]]['next_hop']
                    if selected_hop is None:
                        selected_hop = hop
                    else:
                        self.assertEqual(hop, selected_hop)
