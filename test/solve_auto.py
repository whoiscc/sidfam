from unittest import TestCase
from core import solve_auto, Resource


class TestSolveAuto(TestCase):
    def test_merlin(self):
        def edge(dest, hop, req):
            return {
                'dest': dest,
                'trans': {
                    'src': lambda ip: ip == '<ip0>' or ip == '<ip3>',
                    'dst': lambda ip: ip == '<ip2>',
                    'pkt': [], 'spg': [], 'req': req, 'spu': [],
                    'hop': hop
                }
            }
        
        network = {
            'ip': {
                '<ip0>': 'A',
                '<ip1>': 'B',
                '<ip2>': 'C',
                '<ip3>': 'D',
            },
            'conn': {
                'A': ['B', 'D'],
                'B': ['A', 'C', 'D'],
                'C': ['B', 'D'],
                'D': ['A', 'B', 'C'],
            },
        }
        R = Resource
        bandwidth = {
            'A': {'B': R(20), 'D': R(10)},
            'B': {'A': R(10), 'C': R(20), 'D': R(5)},
            'C': {'B': R(10), 'D': R(10)},
            'D': {'A': R(10), 'B': R(5), 'C': R(10)},
        }

        no_req = lambda env: {}
        def bw_req(x):
            return lambda env: {
                bandwidth[env['current_hop']][env['next_hop']]: x
            }

        auto = {
            'initial': 'q0', 'end': 'qe',
            'nodes': {
                'q0': [
                    edge('q0', '.', bw_req(10)),
                    edge('q1', 'B', bw_req(10)),
                ],
                'q1': [
                    edge('q1', '.', bw_req(10)),
                    edge('qe', 'e', no_req),
                ],
            },
        }

        rule = solve_auto(auto, network)
        # same as above
        expect_rule = {
            'A': [
                {
                    'match': {
                        'src': '<ip0>', 'dst': '<ip2>', 'pkt': [], 'spg': []
                    }, 
                    'action': {
                        'spu': [], 'next_hop': 'B'
                    }
                }, {
                    'match': {
                        'src': '<ip3>', 'dst': '<ip2>', 'pkt': [], 'spg': []
                    }, 'action': {
                        'spu': [], 'next_hop': 'B'
                    }
                }
            ], 
            'B': [
                {
                    'match': {
                        'src': '<ip0>', 'dst': '<ip2>', 'pkt': [], 'spg': []
                    }, 
                    'action': {
                        'spu': [], 'next_hop': 'C'
                    }
                }, {
                    'match': {
                        'src': '<ip3>', 'dst': '<ip2>', 'pkt': [], 'spg': []
                    }, 
                    'action': {
                        'spu': [], 'next_hop': 'C'
                    }
                }
            ], 
            'C': [
                {
                    'match': {
                        'src': '<ip0>', 'dst': '<ip2>', 'pkt': [], 'spg': []
                    },
                    'action': {
                        'spu': [], 'next_hop': 'e'
                    }
                }, {
                    'match': {
                        'src': '<ip3>', 'dst': '<ip2>', 'pkt': [], 'spg': []
                    }, 
                    'action': {
                        'spu': [], 'next_hop': 'e'
                    }
                }
            ], 
            'D': [
                {
                    'match': {
                        'src': '<ip3>', 'dst': '<ip2>', 'pkt': [], 'spg': []
                    }, 
                    'action': {
                        'spu': [], 'next_hop': 'A'
                    }
                }
            ]
        }
        self.assertEqual(rule, expect_rule)

    def test_SNAP(self):
        spu = {'dep': {'x'}}
        no_req = lambda env: {}

        def edges(dest, hop, spu):
            return [
                {
                    'dest': dest,
                    'trans': {
                        'src': lambda ip: ip == '<ip0>',
                        'dst': lambda ip: ip == '<ip2>',
                        'pkt': [], 'spg': [], 'req': no_req, 'spu': spu,
                        'hop': hop
                    }
                }, {
                    'dest': dest,
                    'trans': {
                        'src': lambda ip: ip == '<ip1>',
                        'dst': lambda ip: ip == '<ip3>',
                        'pkt': [], 'spg': [], 'req': no_req, 'spu': spu,
                        'hop': hop
                    }
                },
            ]
        
        network = {
            'ip': {
                '<ip0>': 'A',
                '<ip1>': 'B',
                '<ip2>': 'C',
                '<ip3>': 'D',
            },
            'conn': {
                'A': ['B'],
                'B': ['A', 'C', 'D'],
                'C': ['B'],
                'D': ['B'],
            },
        }
        auto = {
            'initial': 'q0', 'end': 'qe',
            'nodes': {
                'q0': edges('q0', '.', []) + \
                    edges('q1', '.', [spu]) + \
                    edges('qe', 'e', [spu]),
                'q1': edges('q1', '.', []) + \
                    edges('qe', 'e', []),
            },
        }

        rule = solve_auto(auto, network)
        # print(rule)
        expect_rule = {
            'C': [
                {
                    'match': {
                        'src': '<ip0>', 'dst': '<ip2>', 'pkt': [], 'spg': []
                    }, 
                    'action': {
                        'spu': [], 'next_hop': 'e'
                    }
                }
            ], 
            'A': [
                {
                    'match': {
                        'src': '<ip0>', 'dst': '<ip2>', 'pkt': [], 'spg': []
                    }, 
                    'action': {
                        'spu': [], 'next_hop': 'B'
                    }
                }
            ], 
            'B': [
                {
                    'match': {
                        'src': '<ip0>', 'dst': '<ip2>', 'pkt': [], 'spg': []
                    }, 
                    'action': {
                        'spu': [spu], 'next_hop': 'C'
                    }
                }, {
                    'match': {
                        'src': '<ip1>', 'dst': '<ip3>', 'pkt': [], 'spg': []
                    }, 
                    'action': {
                        'spu': [spu], 'next_hop': 'D'
                    }
                }
            ], 
            'D': [
                {
                    'match': {
                        'src': '<ip1>', 'dst': '<ip3>', 'pkt': [], 'spg': []
                    }, 
                    'action': {
                        'spu': [], 'next_hop': 'e'
                    }
                }
            ]
        }
        self.assertEqual(rule, expect_rule)
