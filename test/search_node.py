from unittest import TestCase
from core import search_node, compatible_choice_key, same_comp_list, \
    merge_choice_key


class TestSearchNode(TestCase):
    def test_simple(self):
        def comp(n):
            return {
                'str': 'comp str placeholder',
                'dep': [],
                'equ': lambda comp1, comp2: comp1['index'] == comp2['index'],
                'index': n
            }

        def ip(n):
            return f'ip{n} placeholder'

        req = 'req placeholder'
        spu = 'spu placeholder'

        def edge(dest, hop, ck, spg):
            return {
                'dest': dest,
                'sdt': {**ck, 'spg': spg, 'req': req, 'spu': spu, 'hop': hop}
            }

        ck1 = {'src': ip(0), 'dst': ip(1), 'pkt': []}
        ck2 = {'src': ip(0), 'dst': ip(1), 'pkt': [comp(0)]}
        ck3 = {'src': ip(0), 'dst': ip(2), 'pkt': [comp(1)]}
        ck4 = {'src': ip(0), 'dst': ip(2), 'pkt': [comp(2)]}
        ck5 = {'src': ip(1), 'dst': ip(2), 'pkt': []}
        group = {
            'q0': [
                {
                    'ck': ck1,
                    'edges': [
                        # 0A
                        edge('q0', 'A', ck1, [comp(3), comp(4)]),
                        edge('q1', 'B', ck1, [comp(4), comp(3)]),
                    ],
                }, {
                    'ck': ck3,
                    'edges': [
                        # 0B
                        edge('q1', 'C', ck3, []),
                    ],
                }, {
                    'ck': ck5,
                    'edges': [
                        # 0C
                        edge('q1', 'D', ck5, []),
                    ],
                },
            ],
            'q1': [
                {
                    'ck': ck2,
                    'edges': [
                        # 1A, compatible with 0A
                        edge('q2', 'A', ck2, [comp(3)]),
                    ],
                }, {
                    'ck': ck4,
                    'edges': [
                        # 1B, compatible with 0B
                        edge('q3', 'B', ck4, [comp(3)]),
                    ],
                }, {
                    'ck': ck5,
                    'edges': [
                        # 1C, compatible with 0C, ends here
                        edge('q1', 'C', ck5, []),
                    ],
                },
            ],
            'q2': [
                {
                    'ck': ck3,
                    'edges': [
                        # 2A, compatible with 0B, 1B
                        edge('qe', 'e', ck3, []),
                    ],
                }, {
                    'ck': ck1,
                    'edges': [
                        # 2B, compatible with 0A, 1A
                        edge('q3', 'B', ck1, []),
                    ],
                },
            ],
            'q3': [
                {
                    'ck': ck2,
                    'edges': [
                        # 3A, compatible with 0B, 1B, 2A
                        edge('qe', 'e', ck2, []),
                    ],
                }, {
                    'ck': ck4,
                    'edges': [
                        # 3B, compatible with 0A, 1A, 2B
                        edge('q2', 'B', ck4, []),
                    ],
                },
            ],
        }

        for ck in [ck1, ck2, ck3, ck4, None]:
            choice_list = list(search_node(group, 'qe', 'qe', set(), ck))
            self.assertEqual(len(choice_list), 1)
            result_ck, _ = choice_list[0]
            if ck is None:
                self.assertIsNone(result_ck)
            else:
                self.assertTrue(compatible_choice_key(result_ck, ck))
                # print(result_ck)
                self.assertTrue(same_comp_list(result_ck['pkt'], ck['pkt']))
        for ck in [ck1, ck2]:  # 2B
            choice_list = list(search_node(group, 'q2', 'qe', set(), ck))
            self.assertEqual(len(choice_list), 1)
            result_ck, result_choice = choice_list[0]
            expect_ck = merge_choice_key(ck1, ck2)
            self.assertTrue(compatible_choice_key(result_ck, expect_ck))
            self.assertTrue(same_comp_list(result_ck['pkt'], expect_ck['pkt']))
            self.assertEqual(result_choice, {'q2': 1, 'q3': 0, 'qe': -1})
        for ck in [ck3, ck4]:  # 2A
            choice_list = list(search_node(group, 'q2', 'qe', set(), ck))
            self.assertEqual(len(choice_list), 1)
            result_ck, result_choice = choice_list[0]
            expect_ck = merge_choice_key(ck, ck3)
            self.assertTrue(compatible_choice_key(result_ck, expect_ck))
            self.assertTrue(same_comp_list(result_ck['pkt'], expect_ck['pkt']))
            self.assertEqual(set(result_choice.keys()), {'q2', 'qe'})
            self.assertEqual(result_choice['q2'], 0)
        choice_list = list(search_node(group, 'q2', 'qe', set(), None))  # both
        self.assertEqual(len(choice_list), 2)
        first_ck, _ = choice_list[0]
        if compatible_choice_key(first_ck, ck1):  # 2B
            ck2B, choice2B = choice_list[0]
            ck2A, choice2A = choice_list[1]
        else:
            ck2A, choice2A = choice_list[0]
            ck2B, choice2B = choice_list[1]
        expect_ck2A, expect_ck2B = ck3, merge_choice_key(ck1, ck2)
        self.assertTrue(compatible_choice_key(ck2A, expect_ck2A))
        self.assertTrue(same_comp_list(ck2A['pkt'], expect_ck2A['pkt']))
        self.assertTrue(compatible_choice_key(ck2B, expect_ck2B))
        self.assertTrue(same_comp_list(ck2B['pkt'], expect_ck2B['pkt']))
        self.assertEqual(choice2A, {'q2': 0, 'qe': -1})
        self.assertEqual(choice2B, {'q2': 1, 'q3': 0, 'qe': -1})

        choice_list = list(search_node(group, 'q2', 'qe', {'q3'}, None))  # 2A
        self.assertEqual(len(choice_list), 1)
        result_ck, result_choice = choice_list[0]
        expect_ck =ck3
        self.assertTrue(compatible_choice_key(result_ck, expect_ck))
        self.assertTrue(same_comp_list(result_ck['pkt'], expect_ck['pkt']))
        self.assertEqual(set(result_choice.keys()), {'q2', 'qe'})
        self.assertEqual(result_choice['q2'], 0)

        choice_list = list(search_node(group, 'q3', 'qe', set(), None))
        self.assertEqual(len(choice_list), 2)
        choice_list = list(search_node(group, 'q1', 'qe', set(), None))
        self.assertEqual(len(choice_list), 2)
        choice_list = list(search_node(group, 'q0', 'qe', set(), None))
        self.assertEqual(len(choice_list), 2)
        choice_list = list(search_node(group, 'q0', 'q1', set(), None))
        self.assertEqual(len(choice_list), 3)

    def test_merge(self):
        def comp(n):
            return {
                'str': 'comp str placeholder',
                'dep': [],
                'equ': lambda comp1, comp2: comp1['index'] == comp2['index'],
                'index': n
            }

        def ip(n):
            return f'ip{n} placeholder'

        req = 'req placeholder'
        spu = 'spu placeholder'

        def edge(dest, hop, ck, spg):
            return {
                'dest': dest,
                'sdt': {**ck, 'spg': spg, 'req': req, 'spu': spu, 'hop': hop}
            }

        ck1 = {'src': ip(0), 'dst': ip(1), 'pkt': []}
        ck2 = {'src': ip(0), 'dst': ip(1), 'pkt': [comp(0)]}
        ck3 = {'src': ip(0), 'dst': ip(2), 'pkt': [comp(1)]}
        ck4 = {'src': ip(0), 'dst': ip(2), 'pkt': [comp(2)]}
        ck5 = {'src': ip(1), 'dst': ip(2), 'pkt': []}
        group = {
            'q1': [
                {
                    'ck': ck2,
                    'edges': [
                        # 1A, compatible with 0A
                        edge('q2', 'A', ck2, [comp(3)]),
                        # merge 1B into 1A this time
                        edge('q3', 'B', ck2, [comp(3)]),
                    ],
                },
            ],
            'q2': [
                {
                    'ck': ck3,
                    'edges': [
                        # 2A, compatible with 0B, 1B
                        edge('qe', 'e', ck3, []),
                    ],
                }, {
                    'ck': ck1,
                    'edges': [
                        # 2B, compatible with 0A, 1A
                        edge('q3', 'B', ck1, []),
                    ],
                },
            ],
            'q3': [
                {
                    'ck': ck2,
                    'edges': [
                        # 3A, compatible with 0B, 1B, 2A
                        edge('qe', 'e', ck2, []),
                    ],
                }, {
                    'ck': ck4,
                    'edges': [
                        # 3B, compatible with 0A, 1A, 2B
                        edge('q2', 'B', ck4, []),
                    ],
                },
            ],
        }

        choice_list = list(search_node(group, 'q1', 'qe', set(), None))
        # print(choice_list)
        self.assertEqual(len(choice_list), 1)

        group = {
            'q1': [
                {
                    'ck': ck1,
                    'edges': [
                        # 1A
                        edge('q2', 'A', ck1, [comp(3)]),
                        edge('q3', 'B', ck1, [comp(3)]),
                    ],
                },
            ],
            'q2': [
                {
                    'ck': ck1,
                    'edges': [
                        # 2A, compatible with 1A
                        edge('qe', 'e', ck1, []),
                    ],
                }, {
                    'ck': ck2,
                    'edges': [
                        # 2B, compatible with 1A
                        edge('q3', 'B', ck2, []),
                    ],
                },
            ],
            'q3': [
                {
                    'ck': ck2,
                    'edges': [
                        # 3A, compatible with 1A, 2A, 2B
                        edge('qe', 'e', ck2, []),
                    ],
                }, {
                    'ck': ck1,
                    'edges': [
                        # 3B, compatible with 1A, 2A, 2B
                        edge('q2', 'B', ck1, []),
                    ],
                },
            ],
        }

        choice_list = list(search_node(group, 'q1', 'qe', set(), None))
        self.assertEqual(len(choice_list), 3)
        self.assertEqual(
            sorted([sorted(choice.items()) for ck, choice in choice_list]),
            sorted(
                [
                    sorted([('q1', 0), ('q2', 0), ('q3', 0), ('qe', -1)]),
                    sorted([('q1', 0), ('q2', 1), ('q3', 0), ('qe', -1)]),
                    sorted([('q1', 0), ('q2', 0), ('q3', 1), ('qe', -1)]),
                ]
            )
        )

        group = {
            'q1': [
                {
                    'ck': ck1,
                    'edges': [
                        # 1A
                        edge('q2', 'A', ck1, [comp(3)]),
                        edge('q3', 'B', ck1, [comp(3)]),
                    ],
                },
            ],
            'q2': [
                {
                    'ck': ck1,
                    'edges': [
                        # 2A, compatible with 1A
                        edge('q4', 'A', ck1, []),
                    ],
                },
            ],
            'q3': [
                {
                    'ck': ck2,
                    'edges': [
                        # 3A, compatible with 1A, 2A
                        edge('q4', 'A', ck2, []),
                    ],
                },
            ],
            'q4': [
                {
                    'ck': ck1,
                    'edges': [
                        # 4A, compatible with 1A, 2A, 3A
                        edge('qe', 'e', ck1, [])
                    ]
                }, {
                    'ck': ck2,
                    'edges': [
                        # 4A, compatible with 1A, 2A, 3A
                        edge('qe', 'e', ck2, [])
                    ]
                },
            ],
        }

        choice_list = list(search_node(group, 'q1', 'qe', set(), None))
        self.assertEqual(len(choice_list), 2)
        self.assertEqual(
            sorted([sorted(choice.items()) for ck, choice in choice_list]),
            sorted(
                [
                    sorted([('q1', 0), ('q2', 0), ('q3', 0), ('q4', 0), ('qe', -1)]),
                    sorted([('q1', 0), ('q2', 0), ('q3', 0), ('q4', 1), ('qe', -1)]),
                ]
            )
        )
