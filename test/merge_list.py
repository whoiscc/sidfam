from unittest import TestCase
from core import merge_comp_list, same_comp_list


class TestMergeCompList(TestCase):
    def test_simple(self):
        def comp(n):
            return {
                'str': 'comp str placeholder',
                'dep': [],
                'equ': lambda comp1, comp2: comp1['index'] == comp2['index'],
                'index': n
            }

        cl1 = [comp(0), comp(1), comp(2)]
        cl2 = [comp(0), comp(1), comp(3)]
        cl3 = [comp(2), comp(3), comp(4)]
        self.assertTrue(
            same_comp_list(
                merge_comp_list(cl1, cl2),
                [comp(0), comp(1), comp(2), comp(3)],
            ),
        )
        self.assertTrue(
            same_comp_list(
                merge_comp_list(cl2, cl3),
                [comp(0), comp(1), comp(2), comp(3), comp(4)],
            ),
        )
