from unittest import TestCase
from lang import merlin
from core import Resource, solve_auto


class TestMerlin(TestCase):
    def test_simple(self):
        R = Resource
        bandwidth = {}
        for hop1 in ['A', 'B', 'C', 'D']:
            for hop2 in ['A', 'B', 'C', 'D']:
                bandwidth[hop1, hop2] = R(100)
        auto, bw_getter = merlin({
            'path': {
                'x': {
                    'src': lambda ip: ip == '<ip0>',
                    'dst': lambda ip: ip == '<ip2>',
                    'pkt': [], 'regex': {
                        'type': 'sequence', 'value': [
                            {
                                'type': 'star', 'value': {
                                    'type': 'hop', 'value': '.',
                                },
                            },
                            {'type': 'hop', 'value': 'B'},
                            {'type': 'hop', 'value': 'D'},
                            {
                                'type': 'star', 'value': {
                                    'type': 'hop', 'value': '.',
                                },
                            },
                        ],
                    },
                },
            },
            'rule': [],
        })
        bw_getter(bandwidth)
        # from pprint import pprint
        # pprint(auto)
        network = {
            'ip': {
                '<ip0>': 'A',
                '<ip1>': 'B',
                '<ip2>': 'C',
                '<ip3>': 'D',
            },
            'conn': {
                'A': ['B', 'D'],
                'B': ['A', 'C', 'D'],
                'C': ['B', 'D'],
                'D': ['A', 'B', 'C'],
            },
        }
        rule = solve_auto(auto, network)
        for hop in ['A', 'B', 'C', 'D']:
            self.assertIn(hop, rule)
            self.assertEqual(len(rule[hop]), 1)

    def test_bandwidth(self):
        R = Resource
        bandwidth = {}
        for hop1 in ['A', 'B', 'C', 'D']:
            for hop2 in ['A', 'B', 'C', 'D']:
                bandwidth[hop1, hop2] = R(100)
        bandwidth['A', 'B'] = R(0)
        auto, bw_getter = merlin({
            'path': {
                'x': {
                    'src': lambda ip: ip == '<ip0>',
                    'dst': lambda ip: ip == '<ip2>',
                    'pkt': [], 'regex': {
                        'type': 'sequence', 'value': [
                            {
                                'type': 'star', 'value': {
                                    'type': 'hop', 'value': '.',
                                },
                            },
                            {'type': 'hop', 'value': 'B'},
                            {'type': 'hop', 'value': 'D'},
                            {
                                'type': 'star', 'value': {
                                    'type': 'hop', 'value': '.',
                                },
                            },
                        ],
                    },
                },
            },
            'rule': [
                {'type': 'min', 'path': 'x', 'limit': 5},
                {'type': 'min', 'path': 'x', 'limit': 6},
            ],
        })
        bw_getter(bandwidth)
        # from pprint import pprint
        # pprint(auto)
        network = {
            'ip': {
                '<ip0>': 'A',
                '<ip1>': 'B',
                '<ip2>': 'C',
                '<ip3>': 'D',
            },
            'conn': {
                'A': ['B', 'D'],
                'B': ['A', 'C', 'D'],
                'C': ['B', 'D'],
                'D': ['A', 'B', 'C'],
            },
        }
        with self.assertRaises(Exception):
            print(solve_auto(auto, network))
