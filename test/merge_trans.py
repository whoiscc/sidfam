from unittest import TestCase
from core import merge_trans, trans_to_sdt_generator, same_comp_list, Resource


class TestMergeTrans(TestCase):
    def test_simple(self):
        def comp(n):
            return {
                'equ': lambda c1, c2: c1['index'] == c2['index'],
                'index': n,
                'dep': set(),
            }

        t1 = {
            'src': lambda ip: ip == '<ip0>',
            'dst': lambda ip: ip == '<ip1>',
            'pkt': [comp(0)],
            'spg': [comp(1)],
            'req': lambda env: {},
            'spu': [comp(2)],
            'hop': 'A',
        }
        t2 = merge_trans(t1, t1)
        inside = False
        for sdt in trans_to_sdt_generator(t2, ['<ip0>', '<ip1>', '<ip2>']):
            self.assertFalse(inside)
            inside = True
            self.assertEqual(sdt['src'], '<ip0>')
            self.assertEqual(sdt['dst'], '<ip1>')
            self.assertTrue(same_comp_list(sdt['pkt'], t1['pkt']))
            self.assertTrue(same_comp_list(sdt['spg'], t1['spg']))
            self.assertEqual(len(sdt['spu']), 2)
            self.assertEqual(sdt['hop'], 'A')
            self.assertEqual(sdt['req']({}), {})
        self.assertTrue(inside)

    def test_hop(self):
        def comp(n):
            return {
                'equ': lambda c1, c2: c1['index'] == c2['index'],
                'index': n,
                'dep': set(),
            }

        t1 = {
            'src': lambda ip: ip == '<ip0>',
            'dst': lambda ip: ip == '<ip1>',
            'pkt': [comp(0)],
            'spg': [comp(1)],
            'req': lambda env: {},
            'spu': [comp(2)],
            'hop': 'A',
        }

        t2 = {**t1, 'hop': 'B'}
        with self.assertRaises(Exception):
            merge_trans(t1, t2)
        t3 = {**t1, 'hop': '.'}
        t4 = merge_trans(t1, t3)
        self.assertEqual(t4['hop'], 'A')
        t5 = merge_trans(t3, t1)
        self.assertEqual(t5['hop'], 'A')

    def test_req(self):
        def comp(n):
            return {
                'equ': lambda c1, c2: c1['index'] == c2['index'],
                'index': n,
                'dep': set(),
            }

        res1 = {
            'A': {
                'B': Resource(),
            }
        }
        res2 = {'A': Resource()}

        t1 = {
            'src': lambda ip: ip == '<ip0>',
            'dst': lambda ip: ip == '<ip1>',
            'pkt': [comp(0)],
            'spg': [comp(1)],
            'req': lambda env: {
                res1[env['current_hop']][env['next_hop']]: 10, 
                res2[env['current_hop']]: 10
            },
            'spu': [comp(2)],
            'hop': 'A',
        }
        t2 = {
            **t1, 
            'req': lambda env: {res1[env['current_hop']][env['next_hop']]: 20}
        }
        t3 = merge_trans(t1, t2)
        self.assertEqual(
            t3['req']({'current_hop': 'A', 'next_hop': 'B', 'other': '<other>'}), 
            {res1['A']['B']: 30, res2['A']: 10}
        )
