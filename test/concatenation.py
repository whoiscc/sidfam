from unittest import TestCase
from oper import concatenation
from core import Resource, solve_auto


class TestConcatenation(TestCase):
    def test_simple(self):
        network = {
            'ip': {
                '<ip0>': 'A',
                '<ip1>': 'B',
                '<ip2>': 'C',
                '<ip3>': 'D',
            },
            'conn': {
                'A': ['B', 'D'],
                'B': ['A', 'C', 'D'],
                'C': ['B', 'D'],
                'D': ['A', 'B', 'C'],
            },
        }

        def edge(dest, hop, req):
            return {
                'dest': dest,
                'trans': {
                    'src': lambda ip: ip == '<ip0>',
                    'dst': lambda ip: ip != '<ip1>',
                    'pkt': [], 'spg': [], 'req': req, 'spu': [],
                    'hop': hop
                }
            }

        R = Resource
        bandwidth = {
            'A': {'B': R(10), 'D': R(100)},
            'B': {'A': R(100), 'C': R(100), 'D': R(100)},
            'C': {'B': R(100), 'D': R(100)},
            'D': {'A': R(100), 'B': R(100), 'C': R(100)},
        }

        no_req = lambda env: {}
        def bw_req(x):
            return lambda env: {
                bandwidth[env['current_hop']][env['next_hop']]: x
            }

        auto1 = {
            'initial': 'q0', 'end': 'qe',
            'nodes': {
                'q0': [
                    edge('q0', '.', bw_req(10)),
                    edge('q1', 'B', bw_req(10)),
                    {
                        'dest': 'qe', 'trans': {
                            'src': lambda ip: ip == '<ip0>',
                            'dst': lambda ip: ip == '<ip1>',
                            'pkt': [], 'spg': [], 'spu': [], 'req': lambda env: {},
                            'hop': 'd',
                        }
                    }
                ],
                'q1': [
                    edge('q1', '.', bw_req(10)),
                    edge('qe', 'e', no_req),
                ],
            },
        }

        spu = {'dep': {'x'}}
        # no_req = lambda env: {}

        pkt = {
            'str': 'dst_port == 53', 
            'equ': lambda c1, c2: c1 is c2,
            'dep': set(),
        }
        def edge(dest, hop, spu):
            return {
                'dest': dest,
                'trans': {
                    'src': lambda ip: True,
                    'dst': lambda ip: True,
                    'pkt': [pkt], 'spg': [], 'req': no_req, 'spu': spu,
                    'hop': hop
                }
            }
        
        auto2 = {
            'initial': 'q0', 'end': 'qe',
            'nodes': {
                'q0': [
                    edge('q0', '.', []),
                    edge('q1', '.', [spu]),
                    edge('qe', 'e', [spu]),
                ],
                'q1': [
                    edge('q1', '.', []),
                    edge('qe', 'e', []),
                ],
            },
        }

        auto3 = concatenation(auto1, auto2)
        self.assertEqual(len(auto3['nodes'].keys() | set([auto3['end']])), 5)
        rule = solve_auto(auto3, network)
        for hop in ['A', 'B', 'C', 'D']:
            self.assertIn(hop, rule)
            if hop != 'A':
                self.assertEqual(len(rule[hop]), 2)
            else:
                self.assertEqual(len(rule[hop]), 3)
            for ma in rule[hop]:
                if hop != 'C':
                    self.assertListEqual(ma['action']['spu'], [])
                else:
                    self.assertListEqual(ma['action']['spu'], [spu])
        self.assertIn({
            'match': {'src': '<ip0>', 'dst': '<ip1>', 'pkt': [], 'spg': []},
            'action': {'spu': [], 'next_hop': 'd'},
        }, rule['A'])
        for ma in rule['B']:
            self.assertEqual(ma['action']['next_hop'], 'C')
