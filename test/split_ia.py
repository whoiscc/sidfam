from unittest import TestCase
from core import split_ia, same_comp_list


class TestSplitIA(TestCase):
    def test_simple(self):
        def comp(n):
            return {
                'str': 'comp str placeholder',
                'dep': [],
                'equ': lambda comp1, comp2: comp1['index'] == comp2['index'],
                'index': n
            }

        def ip(n):
            return f'ip{n} placeholder'

        req = 'req placeholder'
        spu = 'spu placeholder'

        def edge(dest, hop, ck, spg):
            return {
                'dest': dest,
                'sdt': {**ck, 'spg': spg, 'req': req, 'spu': spu, 'hop': hop},
            }

        ck1 = {'src': ip(0), 'dst': ip(1), 'pkt': []}
        ck2 = {'src': ip(0), 'dst': ip(1), 'pkt': [comp(0)]}
        ck3 = {'src': ip(0), 'dst': ip(2), 'pkt': [comp(1)]}
        ck4 = {'src': ip(0), 'dst': ip(2), 'pkt': [comp(2)]}
        ck5 = {'src': ip(1), 'dst': ip(2), 'pkt': []}

        ia = {
            'initial': 'q0', 'end': 'qe',
            'nodes': {
                'q0': [
                    # 0A
                    edge('q0', 'A', ck1, [comp(3), comp(4)]),
                    edge('q1', 'B', ck1, [comp(4), comp(3)]),
                    # 0B
                    edge('q1', 'C', ck3, [comp(3)]),
                    # 0C
                    edge('q0', 'D', ck5, []),
                ],
                'q1': [
                    # 1A
                    edge('q2', 'A', ck2, [comp(3)]),
                    # 1B
                    edge('q3', 'B', ck4, [comp(3)]),
                    # 1C
                    edge('q1', 'C', ck5, []),
                ],
                'q2': [
                    edge('qe', 'e', ck3, []),  # 2A
                    edge('q3', 'B', ck1, []),  # 2B
                ],
                'q3': [
                    edge('qe', 'e', ck2, []),  # 3A
                    edge('q2', 'B', ck4, []),  # 3B
                ],
            },
        }

        fia_list = list(split_ia(ia))
        self.assertEqual(len(fia_list), 2)
        if fia_list[0]['dst'] == ip(1):
            fia1 = fia_list[0]
            fia2 = fia_list[1]
        else:
            fia1 = fia_list[1]
            fia2 = fia_list[0]
        self.assertEqual(fia1['src'], ip(0))
        self.assertEqual(fia2['src'], ip(0))
        self.assertEqual(fia2['dst'], ip(2))
        dest1 = {'q0': ['q0', 'q1'], 'q1': ['q2'], 'q2': ['q3'], 'q3': ['qe']}
        for node in fia1['ia']['nodes']:
            for edge in fia1['ia']['nodes'][node]:
                self.assertTrue(
                    same_comp_list(edge['sdt']['pkt'], [comp(0)]),
                )
                self.assertIn(edge['dest'], dest1[node])
        dest2 = {'q0': ['q1'], 'q1': ['q3'], 'q3': ['q2'], 'q2': ['qe']}
        for node in fia2['ia']['nodes']:
            for edge in fia2['ia']['nodes'][node]:
                self.assertTrue(
                    same_comp_list(edge['sdt']['pkt'], [comp(1), comp(2)]),
                )
                self.assertIn(edge['dest'], dest2[node])

    def test_more(self):
        def comp(n):
            return {
                'str': 'comp str placeholder',
                'dep': [],
                'equ': lambda comp1, comp2: comp1['index'] == comp2['index'],
                'index': n
            }

        def ip(n):
            return f'ip{n} placeholder'

        req = 'req placeholder'
        spu = 'spu placeholder'

        def edge(dest, hop, ck, spg):
            return {
                'dest': dest,
                'sdt': {**ck, 'spg': spg, 'req': req, 'spu': spu, 'hop': hop},
            }

        ck1 = {'src': ip(0), 'dst': ip(1), 'pkt': []}
        
        # This ia may look weird to you, because `split_ia` is updated after
        # the creation of this testcase.
        # It used to group 0A and 0B into different edge groups, and generated
        # 8 fia instead of 3. The new version generates a smaller result and
        # may be quicker to run.
        ia = {
            'initial': 'q0', 'end': 'qe',
            'nodes': {
                'q0': [
                    # 0A
                    edge('q0', 'A', ck1, [comp(3), comp(4)]),
                    edge('q1', 'B', ck1, [comp(4), comp(3)]),
                    # 0B
                    edge('q1', 'C', ck1, [comp(3)]),
                ],
                'q1': [
                    edge('q2', 'A', ck1, [comp(5)]),  # 1A
                    edge('q3', 'B', ck1, [comp(6)]),  # 1B
                    edge('q1', 'C', ck1, []), # 1C, ends here
                ],
                'q2': [
                    edge('qe', 'e', ck1, [comp(7)]),  # 2A
                    edge('q3', 'B', ck1, [comp(8)]),  # 2B
                ],
                'q3': [
                    edge('qe', 'e', ck1, [comp(9)]),  # 3A
                    edge('q2', 'B', ck1, []),  # 3B
                ],
            },
        }

        fia_list = list(split_ia(ia))
        # from pprint import pprint
        # pprint(fia_list)
        self.assertEqual(len(fia_list), 3)
