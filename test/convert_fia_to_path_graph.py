from unittest import TestCase
from core import convert_fia_to_path_graph


class TestConvertFIAToPathGraph(TestCase):
    def test_simple(self):
        def ip(n):
            return f'ip{n} placeholder'

        def edge(dest, hop):
            return {
                'dest': dest,
                'sdt': {
                    'hop': hop, 
                    **{
                        key: f'{key} placeholder' 
                        for key in ['src', 'dst', 'pkt', 'spg', 'req', 'spu']
                    },
                },
            }

        fia = {
            'src': ip(0), 'dst': ip(1),
            'ia': {
                'initial': 'q0', 'end': 'qe',
                'nodes': {
                    'q0': [
                        edge('q0', 'D'),
                        edge('q1', 'B'),
                    ],
                    'q1': [
                        edge('q0', 'C'),
                        edge('q1', 'A'),
                        edge('qe', 'e'),
                    ],
                },
            },
        }

        nw = {
            'ip': {
                ip(0): 'A',
                ip(1): 'B',
                ip(2): 'C',
                ip(3): 'D',
            },
            'conn': {
                'A': ['B', 'D'],
                'B': ['A', 'C', 'D'],
                'C': ['B', 'D'],
                'D': ['A', 'B', 'C'],
            },
        }

        pg = convert_fia_to_path_graph(fia, nw)
        self.assertEqual(len(pg['vert']), 6)
        self.assertEqual(len(pg['edge']), 8)
        v0A = 0
        self.assertEqual(pg['vert'][v0A], {'next_hop': 'A'})
        v0D, v1B = None, None
        for edge in pg['edge']:
            s, d = edge
            if pg['vert'][s] == pg['vert'][0]:
                if pg['vert'][d]['next_hop'] == 'B':
                    v1B = d
                elif pg['vert'][d]['next_hop'] == 'D':
                    v0D = d
                else:
                    self.assertFalse(pg['vert'][d])
        self.assertIsNotNone(v0D)
        self.assertIsNotNone(v1B)
        v1A, v2e, v0C = None, None, None
        for edge in pg['edge']:
            s, d = edge
            if pg['vert'][s] == pg['vert'][v1B]:
                if pg['vert'][d]['next_hop'] == 'A':
                    v1A = d
                elif pg['vert'][d]['next_hop'] == 'e':
                    v2e = d
                elif pg['vert'][d]['next_hop'] == 'C':
                    v0C = d
                else:
                    self.assertFalse(pg['vert'][d])
        self.assertIsNotNone(v1A)
        self.assertIsNotNone(v2e)
        self.assertIsNotNone(v0C)
        self.assertEqual(
            sorted(pg['edge']),
            sorted([
                (v0A, v0D), (v0A, v1B), (v0D, v1B), (v0C, v1B),
                (v1B, v0C), (v1B, v1A), (v1B, v2e), (v0C, v0D),
            ])
        )
