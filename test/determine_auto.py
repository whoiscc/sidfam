from unittest import TestCase
from core import determine_auto


class TestDetermineAuto(TestCase):
    def test_simple(self):
        def trans(hop):
            return {'other': 'other placeholder', 'hop': hop}
        auto = {
            'initial': 'q0',
            'end': 'qe',
            'nodes': {
                'q0': [
                    {'trans': 'epsilon', 'dest': 'q1'},
                ],
                'q1': [
                    {'trans': trans('A'), 'dest': 'q1'},
                    {'trans': trans('e'), 'dest': 'qe'},
                ],
            },
        }
        result = determine_auto(auto)
        self.assertEqual(len(result['nodes'].keys() | set([result['end']])), 3)
        q0 = result['initial']
        qe = result['end']
        q1 = list(result['nodes'].keys() - {q0, qe})[0]
        self.assertEqual(
            [(edge['trans'], edge['dest']) for edge in result['nodes'][q0]],
            [(trans('A'), q1), (trans('e'), qe)],
        )
        self.assertEqual(
            [(edge['trans'], edge['dest']) for edge in result['nodes'][q1]],
            [(trans('A'), q1), (trans('e'), qe)],
        )
        self.assertTrue(qe not in result['nodes'] or \
            len(result['nodes'][qe]) == 0)
