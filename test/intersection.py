from unittest import TestCase
from oper import intersection
from core import solve_auto, Resource


class TestIntersection(TestCase):
    def test_simple(self):
        network = {
            'ip': {
                '<ip0>': 'A',
                '<ip1>': 'B',
                '<ip2>': 'C',
                '<ip3>': 'D',
            },
            'conn': {
                'A': ['B', 'D'],
                'B': ['A', 'C', 'D'],
                'C': ['B', 'D'],
                'D': ['A', 'B', 'C'],
            },
        }

        def edge(dest, hop, req):
            return {
                'dest': dest,
                'trans': {
                    'src': lambda ip: ip != '<ip1>',
                    'dst': lambda ip: True,
                    'pkt': [], 'spg': [], 'req': req, 'spu': [],
                    'hop': hop
                }
            }

        R = Resource
        bandwidth = {
            'A': {'B': R(10), 'D': R(20)},
            'B': {'A': R(10), 'C': R(20), 'D': R(100)},
            'C': {'B': R(100), 'D': R(100)},
            'D': {'A': R(100), 'B': R(20), 'C': R(30)},
        }

        no_req = lambda env: {}
        def bw_req(x):
            return lambda env: {
                bandwidth[env['current_hop']][env['next_hop']]: x
            }

        auto1 = {
            'initial': 'q0', 'end': 'qe',
            'nodes': {
                'q0': [
                    edge('q0', '.', bw_req(10)),
                    edge('q1', 'B', bw_req(10)),
                ],
                'q1': [
                    edge('q1', '.', bw_req(10)),
                    edge('qe', 'e', no_req),
                ],
            },
        }

        spu = {'dep': {'x'}}
        # no_req = lambda env: {}

        pkt = {
            'str': 'dst_port == 53', 
            'equ': lambda c1, c2: c1 is c2,
            'dep': set(),
        }
        def edge(dest, hop, spu):
            return {
                'dest': dest,
                'trans': {
                    'src': lambda ip: True,
                    'dst': lambda ip: True,
                    'pkt': [pkt], 'spg': [], 'req': no_req, 'spu': spu,
                    'hop': hop
                }
            }
        
        auto2 = {
            'initial': 'q0', 'end': 'qe',
            'nodes': {
                'q0': [
                    edge('q0', '.', []),
                    edge('q1', '.', [spu]),
                    edge('qe', 'e', [spu]),
                ],
                'q1': [
                    edge('q1', '.', []),
                    edge('qe', 'e', []),
                ],
            },
        }

        auto3 = intersection(auto1, auto2)
        rule = solve_auto(auto3, network)

        self.assertEqual(len(rule['B']), 9)
        dep_hop = None
        dep_count = 0
        for hop, ma_list in rule.items():
            for ma in ma_list:
                if ma['action']['spu'] != []:
                    if dep_hop is None:
                        dep_hop = hop
                    else:
                        self.assertEqual(dep_hop, hop)
                    dep_count += 1
        self.assertEqual(dep_count, 9)
        bw_usage = {}
        for hop, ma_list in rule.items():
            for ma in ma_list:
                src_hop, dst_hop = hop, ma['action']['next_hop']
                if dst_hop == 'e':
                    continue
                if (src_hop, dst_hop) not in bw_usage:
                    bw_usage[src_hop, dst_hop] = 0
                bw_usage[src_hop, dst_hop] += 10
        for src_hop, dst_hop in bw_usage:
            self.assertIn(src_hop, bandwidth)
            self.assertIn(dst_hop, bandwidth[src_hop])
            self.assertLessEqual(
                bw_usage[src_hop, dst_hop], 
                bandwidth[src_hop][dst_hop].get_ration()
            )

        # reverse instersection
        auto3 = intersection(auto2, auto1)
        rule = solve_auto(auto3, network)

        self.assertEqual(len(rule['B']), 9)
        dep_hop = None
        dep_count = 0
        for hop, ma_list in rule.items():
            for ma in ma_list:
                if ma['action']['spu'] != []:
                    if dep_hop is None:
                        dep_hop = hop
                    else:
                        self.assertEqual(dep_hop, hop)
                    dep_count += 1
        self.assertEqual(dep_count, 9)
        bw_usage = {}
        for hop, ma_list in rule.items():
            for ma in ma_list:
                src_hop, dst_hop = hop, ma['action']['next_hop']
                if dst_hop == 'e':
                    continue
                if (src_hop, dst_hop) not in bw_usage:
                    bw_usage[src_hop, dst_hop] = 0
                bw_usage[src_hop, dst_hop] += 10
        for src_hop, dst_hop in bw_usage:
            self.assertIn(src_hop, bandwidth)
            self.assertIn(dst_hop, bandwidth[src_hop])
            self.assertLessEqual(
                bw_usage[src_hop, dst_hop], 
                bandwidth[src_hop][dst_hop].get_ration()
            )
