from unittest import TestCase
from core import convert_solution_to_rule, create_problem, \
    convert_fia_to_path_graph, build_problem_path, build_problem_deps, solve_problem


class TestConvertSolutionToRule(TestCase):
    def test_simple(self):
        def edges(dest, spu, hop_list, src, dst):
            return [
                {
                    'dest': dest,
                    'sdt': {
                        'src': src, 'dst': dst, 'hop': hop,
                        'pkt': [], 'spg': [], 'req': '<req>', 'spu': spu,
                    }
                }
                for hop in hop_list
            ]

        spu = {'dep': {'x'}}
        hops = ['A', 'B', 'C', 'D']

        def ia(src, dst):
            return {
                'initial': 'q0', 'end': 'qe',
                'nodes': {
                    'q0': edges('q0', [], hops, src, dst) + \
                        edges('q1', [spu], hops, src, dst) + \
                        edges('q2', [spu], ['e'], src, dst),
                    'q1': edges('q1', [], hops, src, dst) + \
                        edges('q2', [], ['e'], src, dst)
                },
            }
        fia_list = [
            {'src': '<ip0>', 'dst': '<ip1>', 'ia': ia('<ip0>', '<ip1>')},
            {'src': '<ip0>', 'dst': '<ip3>', 'ia': ia('<ip0>', '<ip3>')},
        ]
        nw = {
            'ip': {
                '<ip0>': 'A',
                '<ip1>': 'B',
                '<ip2>': 'C',
                '<ip3>': 'D',
            },
            'conn': {
                'A': ['B', 'D'],
                'B': ['A', 'C', 'D'],
                'C': ['B', 'D'],
                'D': ['A', 'B', 'C'],
            },
        }

        pb = create_problem()
        pgpv = []
        # from pprint import pprint
        for fia in fia_list:
            pg = convert_fia_to_path_graph(fia, nw)
            # pprint(pg['vert'])
            pgpv.append(build_problem_path(pb, pg))
        build_problem_deps(pgpv)
        # print(pb)
        sl = solve_problem(pb)
        # pprint(sl)
        rl = convert_solution_to_rule(sl, pgpv)
        # pprint(rl)
        for hop in ['A', 'B', 'D']:
            self.assertIn(hop, rl)
        for ma in rl['B']:
            if ma['match']['dst'] == '<ip1>':
                self.assertEqual(ma['action']['next_hop'], 'e')
        for ma in rl['D']:
            if ma['match']['dst'] == '<ip3>':
                self.assertEqual(ma['action']['next_hop'], 'e')
