from unittest import TestCase
from core import build_problem_req, Resource, convert_fia_to_path_graph, \
    build_problem_path, build_problem_deps, convert_solution_to_rule
from prob import create_problem, solve_problem


class TestBuildProblemReq(TestCase):
    def test_simple(self):
        def edges(dest, hop_list, req, src, dst):
            return [
                {
                    'dest': dest,
                    'sdt': {
                        'src': src, 'dst': dst, 'hop': hop,
                        'pkt': [], 'spg': [], 'req': req, 'spu': [],
                    }
                }
                for hop in hop_list
            ]
        hops = ['A', 'B', 'C', 'D']
        nw = {
            'ip': {
                '<ip0>': 'A',
                '<ip1>': 'B',
                '<ip2>': 'C',
                '<ip3>': 'D',
            },
            'conn': {
                'A': ['B', 'D'],
                'B': ['A', 'C', 'D'],
                'C': ['B', 'D'],
                'D': ['A', 'B', 'C'],
            },
        }
        R = Resource
        bw = {
            'A': {'B': R(10), 'D': R(10)},
            'B': {'A': R(10), 'C': R(10), 'D': R(10)},
            'C': {'B': R(10), 'D': R(10)},
            'D': {'A': R(10), 'B': R(10), 'C': R(10)},
        }

        no_req = lambda env: {}
        def bw_req(x):
            return lambda env: {bw[env['current_hop']][env['next_hop']]: x}

        def ia(src, dst):
            return {
                'initial': 'q0', 'end': 'qe',
                'nodes': {
                    'q0': edges('q0', hops, bw_req(10), src, dst) + \
                        edges('q1', ['B'], bw_req(10), src, dst),
                    'q1': edges('q1', hops, bw_req(10), src, dst) + \
                        edges('qe', ['e'], no_req, src, dst)
                },
            }
        def fia(src, dst):
            return {'src': src, 'dst': dst, 'ia': ia(src, dst)}
        
        fia_list = [fia('<ip0>', '<ip2>'), fia('<ip1>', '<ip2>')]
        pgpv_list = []
        pb = create_problem()
        for fia in fia_list:
            pg = convert_fia_to_path_graph(fia, nw)
            pgpv_list.append(build_problem_path(pb, pg))
        build_problem_deps(pgpv_list)
        build_problem_req(pgpv_list)
        with self.assertRaises(Exception):
            solve_problem(pb)
        # rl = convert_solution_to_rule(sl, pgpv_list)
        # from pprint import pprint
        # pprint(rl)

    def test_solvable(self):
        def edges(dest, hop_list, req, src, dst):
            return [
                {
                    'dest': dest,
                    'sdt': {
                        'src': src, 'dst': dst, 'hop': hop,
                        'pkt': [], 'spg': [], 'req': req, 'spu': [],
                    }
                }
                for hop in hop_list
            ]
        hops = ['A', 'B', 'C', 'D']
        nw = {
            'ip': {
                '<ip0>': 'A',
                '<ip1>': 'B',
                '<ip2>': 'C',
                '<ip3>': 'D',
            },
            'conn': {
                'A': ['B', 'D'],
                'B': ['A', 'C', 'D'],
                'C': ['B', 'D'],
                'D': ['A', 'B', 'C'],
            },
        }
        R = Resource
        bw = {
            'A': {'B': R(20), 'D': R(10)},
            'B': {'A': R(10), 'C': R(20), 'D': R(5)},
            'C': {'B': R(10), 'D': R(10)},
            'D': {'A': R(10), 'B': R(5), 'C': R(10)},
        }

        no_req = lambda env: {}
        def bw_req(x):
            return lambda env: {bw[env['current_hop']][env['next_hop']]: x}

        def ia(src, dst):
            return {
                'initial': 'q0', 'end': 'qe',
                'nodes': {
                    'q0': edges('q0', hops, bw_req(10), src, dst) + \
                        edges('q1', ['B'], bw_req(10), src, dst),
                    'q1': edges('q1', hops, bw_req(10), src, dst) + \
                        edges('qe', ['e'], no_req, src, dst)
                },
            }
        def fia(src, dst):
            return {'src': src, 'dst': dst, 'ia': ia(src, dst)}
        
        fia_list = [fia('<ip0>', '<ip2>'), fia('<ip3>', '<ip2>')]
        pgpv_list = []
        pb = create_problem()
        for fia in fia_list:
            pg = convert_fia_to_path_graph(fia, nw)
            pgpv_list.append(build_problem_path(pb, pg))
        build_problem_deps(pgpv_list)
        build_problem_req(pgpv_list)
        sl = solve_problem(pb)
        rl = convert_solution_to_rule(sl, pgpv_list)
        # print(rl)
        expect_rl = {
            'A': [
                {
                    'match': {
                        'src': '<ip0>', 'dst': '<ip2>', 'pkt': [], 'spg': []
                    }, 
                    'action': {
                        'spu': [], 'next_hop': 'B'
                    }
                }, {
                    'match': {
                        'src': '<ip3>', 'dst': '<ip2>', 'pkt': [], 'spg': []
                    }, 'action': {
                        'spu': [], 'next_hop': 'B'
                    }
                }
            ], 
            'B': [
                {
                    'match': {
                        'src': '<ip0>', 'dst': '<ip2>', 'pkt': [], 'spg': []
                    }, 
                    'action': {
                        'spu': [], 'next_hop': 'C'
                    }
                }, {
                    'match': {
                        'src': '<ip3>', 'dst': '<ip2>', 'pkt': [], 'spg': []
                    }, 
                    'action': {
                        'spu': [], 'next_hop': 'C'
                    }
                }
            ], 
            'C': [
                {
                    'match': {
                        'src': '<ip0>', 'dst': '<ip2>', 'pkt': [], 'spg': []
                    },
                    'action': {
                        'spu': [], 'next_hop': 'e'
                    }
                }, {
                    'match': {
                        'src': '<ip3>', 'dst': '<ip2>', 'pkt': [], 'spg': []
                    }, 
                    'action': {
                        'spu': [], 'next_hop': 'e'
                    }
                }
            ], 
            'D': [
                {
                    'match': {
                        'src': '<ip3>', 'dst': '<ip2>', 'pkt': [], 'spg': []
                    }, 
                    'action': {
                        'spu': [], 'next_hop': 'A'
                    }
                }
            ]
        }
        self.assertEqual(rl, expect_rl)
