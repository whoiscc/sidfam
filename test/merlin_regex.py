from unittest import TestCase
from lang import merlin_regex


class TestMerlinRegex(TestCase):
    def test_simple(self):
        self.assertEqual(
            merlin_regex('<.>*<B><.>*', {}),
            {
                'type': 'sequence', 
                'value': [
                    {
                        'type': 'star', 'value': {
                            'type': 'hop', 'value': '.',
                        },
                    },
                    {'type': 'hop', 'value': 'B'},
                    {
                        'type': 'star', 'value': {
                            'type': 'hop', 'value': '.',
                        },
                    },
                ],
            }
        )
        self.assertEqual(
            merlin_regex('<.>*<B><D><.>*', {}),
            {
                'type': 'sequence', 
                'value': [
                    {
                        'type': 'star', 'value': {
                            'type': 'hop', 'value': '.',
                        },
                    },
                    {'type': 'hop', 'value': 'B'},
                    {'type': 'hop', 'value': 'D'},
                    {
                        'type': 'star', 'value': {
                            'type': 'hop', 'value': '.',
                        },
                    },
                ],
            }
        )
        self.assertEqual(
            merlin_regex('<.>*(<B><.>)*', {}),
            {
                'type': 'sequence', 
                'value': [
                    {
                        'type': 'star', 'value': {
                            'type': 'hop', 'value': '.',
                        },
                    },
                    {
                        'type': 'star', 'value': {
                            'type': 'sequence', 'value': [
                                {'type': 'hop', 'value': 'B'},
                                {'type': 'hop', 'value': '.'},
                            ],
                        }
                    }
                ],
            }
        )
        self.assertEqual(
            merlin_regex('<.>* [A, B]*', {}),
            {
                'type': 'sequence', 
                'value': [
                    {
                        'type': 'star', 'value': {
                            'type': 'hop', 'value': '.',
                        },
                    },
                    {
                        'type': 'star', 'value': {
                            'type': 'or', 'value': [
                                {'type': 'hop', 'value': 'A'},
                                {'type': 'hop', 'value': 'B'},
                            ],
                        }
                    }
                ],
            }
        )
        self.assertEqual(
            merlin_regex('<.> | [^ A, B]*', {}),
            {
                'type': 'or', 
                'value': [
                    {'type': 'hop', 'value': '.'},
                    {
                        'type': 'star', 'value': {
                            'type': 'exclude', 'value': [
                                {'type': 'hop', 'value': 'A'},
                                {'type': 'hop', 'value': 'B'},
                            ],
                        }
                    }
                ],
            }
        )
        auto = 'auto placeholder'
        self.assertEqual(
            merlin_regex('<.> | ([^ A, B]*) | <?dpi>', {'dpi': auto}),
            {
                'type': 'or', 
                'value': [
                    {'type': 'hop', 'value': '.'},
                    {
                        'type': 'star', 'value': {
                            'type': 'exclude', 'value': [
                                {'type': 'hop', 'value': 'A'},
                                {'type': 'hop', 'value': 'B'},
                            ],
                        }
                    },
                    {'type': 'auto', 'value': auto},
                ],
            }
        )
    
    def test_parse_error(self):
        ill_formed_regex = [
            '(<A>',
            '<A>)',
            '<A',
            '<A>>',
            '<A>**',
            '',
            ' ',
            '[<A>',
            '<A> |',
            '<><A>'
        ]
        for regex in ill_formed_regex:
            with self.assertRaises(Exception):
                merlin_regex(regex, {})
